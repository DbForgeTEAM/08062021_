﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO

/* $Header: https://swn-sl01-svn.qvii.net/svn/DB-Test/Aperture/QVI/Programmability/Procedures/dbo.ExtFinExportCustomerToIGFSp.sql 29 2019-10-07 12:07:06Z admsjc@QVI $ */
/*
***************************************************************
*                                                             *
*                           NOTICE                            *
*                                                             *
*   THIS SOFTWARE IS THE PROPERTY OF AND CONTAINS             *
*   CONFIDENTIAL INFORMATION OF INFOR AND/OR ITS AFFILIATES   *
*   OR SUBSIDIARIES AND SHALL NOT BE DISCLOSED WITHOUT PRIOR  *
*   WRITTEN PERMISSION. LICENSED CUSTOMERS MAY COPY AND       *
*   ADAPT THIS SOFTWARE FOR THEIR OWN USE IN ACCORDANCE WITH  *
*   THE TERMS OF THEIR SOFTWARE LICENSE AGREEMENT.            *
*   ALL OTHER RIGHTS RESERVED.                                *
*                                                             *
*   (c) COPYRIGHT 2012 INFOR.  ALL RIGHTS RESERVED.           *
*   THE WORD AND DESIGN MARKS SET FORTH HEREIN ARE            *
*   TRADEMARKS AND/OR REGISTERED TRADEMARKS OF INFOR          *
*   AND/OR ITS AFFILIATES AND SUBSIDIARIES. ALL RIGHTS        *
*   RESERVED.  ALL OTHER TRADEMARKS LISTED HEREIN ARE         *
*   THE PROPERTY OF THEIR RESPECTIVE OWNERS.                  *
*                                                             *
***************************************************************
*/

/* $Archive: /ApplicationDB/Stored Procedures/ExtFinExportCustomerToIGFSp.sp $
 *
 * SL9.01 10 213178 Lqian2 Wed Jun 01 21:48:13 2016
 * Remediate SQL Injection Risk for RC2 List
 * Issue 213178, add quotename
 *
 * SL9.01 9 207078 Kpangan Fri Apr 29 09:38:46 2016
 * Remediate SQL Injection Risk Cat ABC
 * Issue 207078
 *
 * SL9.01 7 RS6298 mguan Wed Dec 23 21:09:25 2015
 * RS6298
 *
 * SL9.01 6 188008 csun Mon Feb 02 22:33:25 2015
 * Associated with RS7090 - Loc - Bank account format
 * Issue#188008
 * RS7090,Change logic to get bank_acct_no and international_bank_account from customer instead of bank_hdr.
 *
 * SL8.04 4 152957 sruffing Fri Sep 07 12:00:38 2012
 * Site analysis code is not applied to IGF record for customer/supplier
 * 152957
 * Assign Site from General Parameters to Analysis Code 0.
 *
 * SL8.04 3 152221 sruffing Tue Aug 21 21:35:11 2012
 * Customer created in multiple SL sites does not created sub-account record in IGF
 * 152221
 * Revised logic to determine value for Sub Account in IGF.
 *
 * SL8.04 2 151831 sruffing Wed Aug 01 16:37:54 2012
 * Customer and Vendor exports fail when Country is not supplied
 * 151831
 * Country is not indicated as a mandatory field in Manual_VEIS.  Country will now use alpha-numeric default value.
 * Should have been pulled to SL8.04 with Issue 151798.
 *
 * SL8.04 1 151319 sruffing Wed Jul 25 14:21:31 2012
 * Issue for RS5443: Integration - Varial Financials and Costing, Part II
 * 148471
 * Added support to create Sub Accounts when the control account varies from existing customer records.
 * Added Bank Header and Address information for Customer Bank Code.
 *
 * $NoKeywords: $
 */
CREATE PROCEDURE [dbo].[ExtFinExportCustomerToIGFSp_copy](
     @CustNum           CustNumType
   , @Infobar           InfobarType    OUTPUT
)
AS

-- Begin of CallALTETPs.exe generated code.
-- Check for existence of alternate definitions (this section was generated and inserted by CallALTETPs.exe):
IF EXISTS (SELECT 1 FROM [optional_module] [om] WHERE ISNULL([om].[is_enabled],0) = 1
AND OBJECT_ID(QUOTENAME(N'ExtFinExportCustomerToIGFSp_' + [om].[ModuleName])) IS NOT NULL)
BEGIN
   DECLARE @ALTGEN TABLE ([SpName] sysname)
   DECLARE @ALTGEN_SpName sysname
   DECLARE @ALTGEN_Severity int

   INSERT INTO @ALTGEN ([SpName])
   SELECT N'ExtFinExportCustomerToIGFSp_' + [om].[ModuleName]
   FROM [optional_module] [om]
   WHERE ISNULL([om].[is_enabled], 0) = 1 AND
   OBJECT_ID(QUOTENAME(N'ExtFinExportCustomerToIGFSp_' + [om].[ModuleName])) IS NOT NULL

   WHILE EXISTS (SELECT 1 FROM @ALTGEN)
   BEGIN
      SELECT TOP 1 @ALTGEN_SpName = [SpName]
      FROM @ALTGEN

      -- Invoke the ALT routine, passing in (and out) this routine's parameters:
      EXEC @ALTGEN_Severity = @ALTGEN_SpName
           @CustNum
         , @Infobar   OUTPUT


      -- ALTGEN routine can RETURN 1 to signal that the remainder of this standard routine should now proceed:
      IF @ALTGEN_Severity <> 1
         RETURN @ALTGEN_Severity

      DELETE @ALTGEN WHERE [SpName] = @ALTGEN_SpName
   END
END
-- End of alternate definitions code.

-- Check for existence of Generic External Touch Point routine (this section was generated and inserted by CallALTETPs.exe):
IF OBJECT_ID(N'dbo.EXTGEN_ExtFinExportCustomerToIGFSp') IS NOT NULL
BEGIN
      DECLARE @EXTGEN_SpName sysname
      SET @EXTGEN_SpName = N'dbo.EXTGEN_ExtFinExportCustomerToIGFSp'
      -- Invoke the ETP routine, passing in (and out) this routine's parameters:
      DECLARE @EXTGEN_Severity int
      EXEC @EXTGEN_Severity = @EXTGEN_SpName
           @CustNum
         , @Infobar   OUTPUT


      -- ETP routine can RETURN 1 to signal that the remainder of this standard routine should now proceed:
      IF @EXTGEN_Severity <> 1
         RETURN @EXTGEN_Severity
END
-- End of Generic External Touch Point code.
-- End of CallALTETPs.exe generated code.



DECLARE
     @Severity             INT
   , @ParmsSite            SiteType
   , @Site                 SiteType
   , @ServerName           OSLocationType
   , @DatabaseName         OSLocationType
   , @CompanyCode          SiteType
   , @Today                DateType

   -- Dynamic SQL Vars
   , @BigDSql                                                  NVARCHAR ( MAX )
   , @BigDSql_2                                                NVARCHAR ( MAX )
   , @DSqlPUBPERSACCBASE_V_SubAccountCheck                     NVARCHAR ( 2000 )
   , @DSqlPUBPERSACCBASE_V_SubAccountCheck_Parms               NVARCHAR ( 500 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER_Parms                NVARCHAR ( 2000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER                      NVARCHAR ( 500 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_Parms                          NVARCHAR ( 2000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_INSERT                         NVARCHAR ( 4000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_VALUES                         NVARCHAR ( 4000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT_Parms NVARCHAR ( 2000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT       NVARCHAR ( 500 )
   , @DSql_PUBPERSACCREPADV_V_GroupExist                       NVARCHAR ( 2000 )
   , @DSql_PUBPERSACCREPADV_V_GroupExist_Parms                 NVARCHAR ( 500 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_CORP_Parms                     NVARCHAR ( 2000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_CORP_INSERT                    NVARCHAR ( 2000 )
   , @DSqlVEIS_PERSONAL_ACCOUNT_CORP_VALUES                    NVARCHAR ( 2000 )

   -- Error Handling
   , @XactAbortSub001                                          NVARCHAR (  100 )
   , @TryCatchSub001                                           NVARCHAR (  100 )
   , @TryCatchSub002                                           NVARCHAR ( 2000 )

-- Enums and Constants
DECLARE
     @MaintainAlphanumericDefault            VARCHAR ( 1 ) = '*'
   , @MaintainNumericDefault                 NUMERIC ( 1 , 0 ) = -1
   , @MaintainDateDefault                    DATE = '02/02/0002'

   -- Technical Field Constants
   , @TFD_PK_VEIS_PERSONAL_ACCOUNT           NUMERIC ( 18 , 0 )
   , @TFD_ENTITYDATE                         DATE               = dbo.GetSiteDate ( GETDATE() )
   , @TFD_ENTITYSTATE                        INTEGER            = 0
   , @TFD_REQNUMBER                          NUMERIC ( 18 , 0 )

  -- PersonalAccountTypeEnum (PAT-Enum)
   , @PATE_CUSTOMER                          NUMERIC ( 2 , 0 )    = 0
   --, @PATE_SUPPLIER                          NUMERIC ( 2 , 0 )    = 1

  -- PaymentTypeEnum (PT-Enum)
   , @PTE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   , @PTE_UNKNOWN                            NUMERIC ( 2 , 0 )    = 1
   , @PTE_BACS                               NUMERIC ( 2 , 0 )    = 2
   , @PTE_CHEQUE                             NUMERIC ( 2 , 0 )    = 3
   , @PTE_DIRECTDEBIT                        NUMERIC ( 2 , 0 )    = 4
   , @PTE_BANKCOLLECTION                     NUMERIC ( 2 , 0 )    = 5
   , @PTE_BILLOFEXCHANGE                     NUMERIC ( 2 , 0 )    = 6
   , @PTE_TRANSFERCHEQUE                     NUMERIC ( 2 , 0 )    = 7
  --, @PTE_CASH                               NUMERIC ( 2 , 0 )    = 8  --Not used by VWE
  --, @PTE_CREDITCARD                         NUMERIC ( 2 , 0 )    = 9  --Not used by VWE
  --, @PTE_STANDINGORDER                      NUMERIC ( 2 , 0 )    = 10 --Not used by VWE
  --, @PTE_INTERCOMPANYALLOCATION             NUMERIC ( 2 , 0 )    = 11 --Not used by VWE
   , @PTE_VIA_ASSOCIATION                    NUMERIC ( 2 , 0 )    = 12
   , @PTE_BACS_BILLOFEXCHANGE                NUMERIC ( 2 , 0 )    = 13
   , @PTE_CHEQUE_BILLOFEXCHANGE              NUMERIC ( 2 , 0 )    = 14
   , @PTE_BILLOFEXCHANGE_ACCEPTED            NUMERIC ( 2 , 0 )    = 15

  -- FactoringTypeEnum (FT-Enum)
   , @FTE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   --, @FTE_NO                                 NUMERIC ( 2 , 0 )    = 1
   --, @FTE_YES                                NUMERIC ( 2 , 0 )    = 2

  -- RitenutaTypeEnum (RT-Enum)
   , @RTE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   --, @RTE_UNKNOWN                            NUMERIC ( 2 , 0 )    = 1
   --, @RTE_COMPANY                            NUMERIC ( 2 , 0 )    = 2
   --, @RTE_PERSON                             NUMERIC ( 2 , 0 )    = 3

  -- SexTypeEnum (ST-Enum)
   , @SE_DEFAULT                             NUMERIC ( 2 , 0 )    = 0
   --, @SE_UNKNOWN                             NUMERIC ( 2 , 0 )    = 1
   --, @SE_FEMALE                              NUMERIC ( 2 , 0 )    = 2
   --, @SE_MALE                                NUMERIC ( 2 , 0 )    = 3

  -- ReferenceTypeEnum (RET-Enum)
   , @RETE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   --, @RETE_BANK                              NUMERIC ( 2 , 0 )    = 1
   --, @RETE_TRADE                             NUMERIC ( 2 , 0 )    = 2
   --, @RETE_AGENCY                            NUMERIC ( 2 , 0 )    = 3
   --, @RETE_OTHER                             NUMERIC ( 2 , 0 )    = 4

  -- CreditLimitTypeEnum (CLT-Enum)
   --, @CLTE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   , @CLTE_CREDIT_LIMIT                      NUMERIC ( 2 , 0 )    = 1
   --, @CLTE_INSURANCE_LIMIT                   NUMERIC ( 2 , 0 )    = 2
   --, @CLTE_INTERNAL_LIMIT                    NUMERIC ( 2 , 0 )    = 3
   --, @CLTE_GROUP_LIMIT                       NUMERIC ( 2 , 0 )    = 4
   --, @CLTE_BILLOFEXCHANGE_LIMIT              NUMERIC ( 2 , 0 )    = 5

  -- RecipientEventEnum (RE-Enum)
   , @REE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   --, @REE_CREDITCONTROL                      NUMERIC ( 2 , 0 )    = 1
   --, @REE_CRITICAL                           NUMERIC ( 2 , 0 )    = 2
   --, @REE_DELIVERYSTOP                       NUMERIC ( 2 , 0 )    = 3
   --, @REE_POSTINGSTOP                        NUMERIC ( 2 , 0 )    = 4
   --, @REE_REMOVE_DELIVERYSTOP                NUMERIC ( 2 , 0 )    = 5

  -- DispatchTypeEnum (DT-Enum)
   , @DTE_DEFAULT                             NUMERIC ( 2 , 0 )   = 0
   --, @DTE_EMAIL                               NUMERIC ( 2 , 0 )   = 1
   --, @DTE_TASK_REMINDER                       NUMERIC ( 2 , 0 )   = 2
   --, @DTE_LETTER                              NUMERIC ( 2 , 0 )   = 3

  -- AgreementPaymentTypeEnum (APT-Enum)
   , @APTE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   --, @APTE_BACS                              NUMERIC ( 2 , 0 )    = 1
   --, @APTE_CHEQUE                            NUMERIC ( 2 , 0 )    = 2
   --, @APTE_CASH                              NUMERIC ( 2 , 0 )    = 3
   --, @APTE_BILLOFEXCHANGE                    NUMERIC ( 2 , 0 )    = 4
   --, @APTE_CREDITCARD                        NUMERIC ( 2 , 0 )    = 5
   --, @APTE_UNKNOWN                           NUMERIC ( 2 , 0 )    = 6
   --, @APTE_DIRECTDEBIT                       NUMERIC ( 2 , 0 )    = 7
   --, @APTE_BANKCOLLECTION                    NUMERIC ( 2 , 0 )    = 8

  -- AgreementStateEnum (AS-Enum)
   , @ASE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   --, @ASE_UNFINISHED                         NUMERIC ( 2 , 0 )    = 1
   --, @ASE_FINISHED                           NUMERIC ( 2 , 0 )    = 2
   --, @ASE_CANCELLED                          NUMERIC ( 2 , 0 )    = 3

  -- ExpenseArrangementTypeEnum (EAT-Enum)
   , @EATE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   --, @EATE_BOTH                              NUMERIC ( 2 , 0 )    = 1
   --, @EATE_SENDER                            NUMERIC ( 2 , 0 )    = 2
   --, @EATE_RECEIVER                          NUMERIC ( 2 , 0 )    = 3

  -- DeclarationTypeEnum (DET-Enum)
   , @DETE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   --, @DETE_NONE                              NUMERIC ( 2 , 0 )    = 1
   --, @DETE_URATE                             NUMERIC ( 2 , 0 )    = 2
   --, @DETE_WRATE                             NUMERIC ( 2 , 0 )    = 3
   --, @DETE_REG                               NUMERIC ( 2 , 0 )    = 4
   --, @DETE_EMI                               NUMERIC ( 2 , 0 )    = 5
   --, @DETE_INF                               NUMERIC ( 2 , 0 )    = 6
   --, @DETE_SNR                               NUMERIC ( 2 , 0 )    = 7

  -- PriorityTypeEnum (PRT-Enum)
   , @PRTE_DEFAULT                           NUMERIC ( 2 , 0 )    = 0
   --, @PRTE_LOW                               NUMERIC ( 2 , 0 )    = 1
   --, @PRTE_MEDIUM                            NUMERIC ( 2 , 0 )    = 2
   --, @PRTE_HIGH                              NUMERIC ( 2 , 0 )    = 3
   --, @PRTE_ESCALATED                         NUMERIC ( 2 , 0 )    = 4

  -- MonthEnum (M-Enum)
   , @ME_DEFAULT                             NUMERIC ( 2 , 0 )    = 0
   --, @ME_JANUARY                             NUMERIC ( 2 , 0 )    = 1
   --, @ME_FEBRUARY                            NUMERIC ( 2 , 0 )    = 2
   --, @ME_MARCH                               NUMERIC ( 2 , 0 )    = 3
   --, @ME_APRIL                               NUMERIC ( 2 , 0 )    = 4
   --, @ME_MAY                                 NUMERIC ( 2 , 0 )    = 5
   --, @ME_JUNE                                NUMERIC ( 2 , 0 )    = 6
   --, @ME_JULY                                NUMERIC ( 2 , 0 )    = 7
   --, @ME_AUGUST                              NUMERIC ( 2 , 0 )    = 8
   --, @ME_SEPTEMBER                           NUMERIC ( 2 , 0 )    = 9
   --, @ME_OCTOBER                             NUMERIC ( 2 , 0 )    = 10
   --, @ME_NOVEMBER                            NUMERIC ( 2 , 0 )    = 11
   --, @ME_DECEMBER                            NUMERIC ( 2 , 0 )    = 12

  -- LimitStatusEnum (LS-Enum)
   , @LSE_DEFAULT                            NUMERIC ( 2 , 0 )    = 0
   --, @LSE_APPROVED                           NUMERIC ( 2 , 0 )    = 1
   --, @LSE_SUBMITTED                          NUMERIC ( 2 , 0 )    = 2
   --, @LSE_OPEN                               NUMERIC ( 2 , 0 )    = 3
   --, @LSE_DEPRECATED                         NUMERIC ( 2 , 0 )    = 4

DECLARE
--VEIS_PERSONAL_ACCOUNT
     @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT           NUMERIC(18,0)
   , @VEIS_PERSONAL_ACCOUNT_ENTITYDATE                         DATE
   , @VEIS_PERSONAL_ACCOUNT_ENTITYSTATE                        NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_REQNUMBER                          NUMERIC(18,0)
   , @VEIS_PERSONAL_ACCOUNT_COMPANYID                          VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE                        NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_PRESETID                           VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_CODE                               VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_SHORTNAME                          VARCHAR(80)
--    , @VEIS_PERSONAL_ACCOUNT_SUNDRYACCOUNT                      CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_PA_NAME                            VARCHAR(160)
   , @VEIS_PERSONAL_ACCOUNT_ADDRESS                            VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_POSTCODE                           VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_TOWN                               VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_COUNTY                             VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_COUNTRY                            VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_VATTAXNUMBER                       VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_ECTAXNUMBER                        VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_LANGUAGEID                         VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_PHONE                              VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_FAX                                VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_EMAIL                              VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_HOMEPAGE                           VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_COMPANYNUMBER                      VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOX                      VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOXPOSTCODE              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_RITENUTATYPE                       NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_CODICEFISCALEORGIURIDICE           VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_TAXOFFICE                          VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_CODICECAUSALE                      VARCHAR(10)
   , @VEIS_PERSONAL_ACCOUNT_TAXREGISTER                        VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_RITENUTATAXCODE                    VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_BIRTHDATE                          DATE
   , @VEIS_PERSONAL_ACCOUNT_BIRTHPLACE                         VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BIRTHCOUNTRY                       VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_FIRSTNAME                          VARCHAR(160)
   , @VEIS_PERSONAL_ACCOUNT_SURNAME                            VARCHAR(160)
   , @VEIS_PERSONAL_ACCOUNT_SEX                                NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_CONTROLACCOUNTID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_DEFAULTACCOUNTID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_CURRENCYID                         VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_TERMSOFPAYMENTID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_TAXCODEID                          VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_MODEOFPAYMENT                      NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_AC0                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC1                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC2                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC3                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC4                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC5                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC6                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC7                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC8                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_AC9                                VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OWNNUMBERATPARTNER                 VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_CTRLACCDOWNPAYMENTID               VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_REPRESENTATIVEID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_ADVISORID                          VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_ISSTOP                             CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_STOPNOTE                           VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_COSTCENTREID                       VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_NEGEXCHANGERATE                    CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_TERMOFPAYMBILLID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_TOLERANCEFORBILLRISK               NUMERIC(4,0)
   , @VEIS_PERSONAL_ACCOUNT_OIAC0                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC1                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC2                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC3                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC4                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC5                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC6                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC7                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC8                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OIAC9                              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_WITHHOLD                           CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_WITHHOLDNOTE                       VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_ISSUBACCOUNT                       CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_EXCLUDEFROMCENTRALPAYMENTS         CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMDAY                NUMERIC(4,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMMONTH              NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILDAY               NUMERIC(4,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILMONTH             NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_TERMSOFPAYMENTID       VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_FACTORING                          NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_FACTORINGNOTE                      VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_CESSIONBANKID                      VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_DATE                     DATE
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_TYPE                     NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_INFORMANT                VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_SHORTADDRESS             VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_PHONE                    VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_REFERENCE_DETAILS                  VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DUNNING_GROUPID                    VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_REMINDER_BYPHONE                   CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_REMINDER_BYLETTER                  CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_REMINDER_BYFAX                     CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_REMINDER_BYEMAIL                   CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOP                      CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOPNOTE                  VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_CREDITINSURANCECODE                VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_INSURANCE_CONTRACTNUMBER           VARCHAR(60)
   , @VEIS_PERSONAL_ACCOUNT_INSURANCE_RETENTION                NUMERIC(6,3)
   , @VEIS_PERSONAL_ACCOUNT_INSURANCE_CUSTOMERNUMBER           VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_FROMDATE                     DATE
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_UNTILDATE                    DATE
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_TYPE                         NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_AMOUNT                       NUMERIC(18,3)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_MAXDAYOVERDUE                NUMERIC(4,0)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_STATUS                       NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_REFUSALREASONID              VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_LIMIT_NOTE                         VARCHAR(120)
   , @VEIS_PERSONAL_ACCOUNT_DELIVERY_REFUSAL_REASONID          VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_SHORTADDRESS             VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_NAME                     VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_ADDRESS                  VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTCODE                 VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTY                   VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTRY                  VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_TOWN                     VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTOFFICEBOX            VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_POBPOSTCODE              VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_PHONE                    VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_FAX                      VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_EMAIL                    VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_LANGUAGEID               VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_COMPANYNUMBER            VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDFROM                DATE
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDUNTIL               DATE
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_EVENTTYPE                NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_DISPATCHTYPE             NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NAME                     VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_ADDRESS                  VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NOTE                     VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_CONTACT                      VARCHAR(160)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_PAYMENTTYPE                  NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_CURRENCYID                   VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_OPENAMOUNT                   NUMERIC(18,3)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDAMOUNT               NUMERIC(18,3)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDDATE                 DATE
   , @VEIS_PERSONAL_ACCOUNT_AGREE_TASKREMINDER                 CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_STATE                        NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_AGREE_NOTE                         VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_NAME                       VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_TOWN                       VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_PHONE                      VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_FAX                        VARCHAR(30)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_EMAIL                      VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_PROFESSION                 VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_LANGUAGEID                 VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_NOTE                       VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_CONTACT_REMINDERDEFAULT            CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_NAME                          VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_SUBJECT                       VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_TEXT                          VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_PRIO                          NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_NEXTPRIO                      NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_NOTE_DATENEXTCONTACT               DATE
   , @VEIS_PERSONAL_ACCOUNT_PAYMEXPENSEARRANGEMENT             NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMDECLARATIONTYPE                NUMERIC(2,0)
   , @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYSHORTNAME               VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYCODE                    VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_PAYMDETAILSCODE                    VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_PAYMDETAILS                        VARCHAR(254)
   , @VEIS_PERSONAL_ACCOUNT_BANKSORTINGCODE                    VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_OTHERREFERENCE                     VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BANK_NAME                          VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_BANK_BRANCH                        VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BANK_ADDRESS                       VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_BANK_POSTCODE                      VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BANK_TOWN                          VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BANK_COUNTY                        VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_BANK_COUNTRY                       VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_BANK_ACCOUNTNUMBER                 VARCHAR(40)
   , @VEIS_PERSONAL_ACCOUNT_DEVIATING_OWNER                    VARCHAR(80)
   , @VEIS_PERSONAL_ACCOUNT_SWIFT                              VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_BANK_INTERNATIONALCODE             VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_POSTCHEQUEACCOUNT                  VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_ABANUMBER                          VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_BANK_CURRENCYID                    VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_BANK_DEFAULT                       CHAR(1)

--VEIS_PERSONAL_ACCOUNT_CORP
   , @VEIS_PERSONAL_ACCOUNT_CORP_PK_VEIS_PERSONAL_ACCOUNT_CORP NUMERIC(18,0)
   , @VEIS_PERSONAL_ACCOUNT_CORP_CORPORATEACCOUNTID            VARCHAR(20)
   , @VEIS_PERSONAL_ACCOUNT_CORP_ISHEADACCOUNT                 CHAR(1)
   , @VEIS_PERSONAL_ACCOUNT_CORP_FK_VEIS_PERSONAL_ACCOUNT      NUMERIC(18,0)

DECLARE
   -- SubAccountCheck Flags
     @IsIGFParentOrNewPersonalAcct                        TINYINT = 0
   , @CustNumNotExistsUnprocessedInVEIS                           TINYINT = 0
   , @IsSubAccount                                             TINYINT = 1 -- Is SubAccount unless proven false

DECLARE
   -- Group Account Existence Flags
     @GroupAcctExists                                          TINYINT = 0
   , @GroupAcctRecDefined                                      TINYINT = 0
   , @IsHeadAccount                                            TINYINT = 0


DECLARE
-- A/R Parameters Accounts
     @ARParmsARAcct                                            AcctType
   , @ARParmsARAcctUnit1                                       UnitCode1Type
   , @ARParmsARAcctUnit2                                       UnitCode2Type
   , @ARParmsARAcctUnit3                                       UnitCode3Type
   , @ARParmsARAcctUnit4                                       UnitCode4Type
   , @ARParmsSalesAcct                                         AcctType
   , @ARParmsSalesAcctUnit1                                    UnitCode1Type
   , @ARParmsSalesAcctUnit2                                    UnitCode2Type
   , @ARParmsSalesAcctUnit3                                    UnitCode3Type
   , @ARParmsSalesAcctUnit4                                    UnitCode4Type
   , @ARParmsMiscAcct                                          AcctType
   , @ARParmsMiscAcctUnit1                                     UnitCode1Type
   , @ARParmsMiscAcctUnit2                                     UnitCode2Type
   , @ARParmsMiscAcctUnit3                                     UnitCode3Type
   , @ARParmsMiscAcctUnit4                                     UnitCode4Type
   , @ARParmsProgAcct                                          AcctType
   , @ARParmsProgAcctUnit1                                     UnitCode1Type
   , @ARParmsProgAcctUnit2                                     UnitCode2Type
   , @ARParmsProgAcctUnit3                                     UnitCode3Type
   , @ARParmsProgAcctUnit4                                     UnitCode4Type
   , @ARParmsSalesDiscAcct                                     AcctType
   , @ARParmsSalesDiscAcctUnit1                                UnitCode1Type
   , @ARParmsSalesDiscAcctUnit2                                UnitCode2Type
   , @ARParmsSalesDiscAcctUnit3                                UnitCode3Type
   , @ARParmsSalesDiscAcctUnit4                                UnitCode4Type
   , @ARParmsProjAcct                                          AcctType
   , @ARParmsProjAcctUnit1                                     UnitCode1Type
   , @ARParmsProjAcctUnit2                                     UnitCode2Type
   , @ARParmsProjAcctUnit3                                     UnitCode3Type
   , @ARParmsProjAcctUnit4                                     UnitCode4Type
   , @ARParmsDraftReceivableAcct                               AcctType
   , @ARParmsDraftReceivableAcctUnit1                          UnitCode1Type
   , @ARParmsDraftReceivableAcctUnit2                          UnitCode2Type
   , @ARParmsDraftReceivableAcctUnit3                          UnitCode3Type
   , @ARParmsDraftReceivableAcctUnit4                          UnitCode4Type
   , @ARParmsNonInvAcct                                        AcctType
   , @ARParmsNonInvAcctUnit1                                   UnitCode1Type
   , @ARParmsNonInvAcctUnit2                                   UnitCode2Type
   , @ARParmsNonInvAcctUnit3                                   UnitCode3Type
   , @ARParmsNonInvAcctUnit4                                   UnitCode4Type

-- End User Type Accounts
   , @EndTypeRowPointer                                        RowPointerType
   , @EndTypeARAcct                                            AcctType
   , @EndTypeARAcctUnit1                                       UnitCode1Type
   , @EndTypeARAcctUnit2                                       UnitCode2Type
   , @EndTypeARAcctUnit3                                       UnitCode3Type
   , @EndTypeARAcctUnit4                                       UnitCode4Type
   , @EndTypeSalesAcct                                         AcctType
   , @EndTypeSalesAcctUnit1                                    UnitCode1Type
   , @EndTypeSalesAcctUnit2                                    UnitCode2Type
   , @EndTypeSalesAcctUnit3                                    UnitCode3Type
   , @EndTypeSalesAcctUnit4                                    UnitCode4Type
   --, @EndTypeSalesDsAcct                                       AcctType
   --, @EndTypeSalesDsAcctUnit1                                  UnitCode1Type
   --, @EndTypeSalesDsAcctUnit2                                  UnitCode2Type
   --, @EndTypeSalesDsAcctUnit3                                  UnitCode3Type
   --, @EndTypeSalesDsAcctUnit4                                  UnitCode4Type
   , @EndTypeDraftReceivableAcct                               AcctType
   , @EndTypeDraftReceivableAcctUnit1                          UnitCode1Type
   , @EndTypeDraftReceivableAcctUnit2                          UnitCode2Type
   , @EndTypeDraftReceivableAcctUnit3                          UnitCode3Type
   , @EndTypeDraftReceivableAcctUnit4                          UnitCode4Type
   --, @EndTypeNonInvAcct                                        AcctType
   --, @EndTypeNonInvAcctUnit1                                   UnitCode1Type
   --, @EndTypeNonInvAcctUnit2                                   UnitCode2Type
   --, @EndTypeNonInvAcctUnit3                                   UnitCode3Type
   --, @EndTypeNonInvAcctUnit4                                   UnitCode4Type

-- Customer
   , @CustomerCustNum                                          CustNumType
   , @CustomerCustSeq                                          CustSeqType
   , @CustomerContact##1                                       ContactType
   , @CustomerContact##2                                       ContactType
   , @CustomerContact##3                                       ContactType
   , @CustomerPhone##1                                         PhoneType
   , @CustomerPhone##2                                         PhoneType
   , @CustomerPhone##3                                         PhoneType
   , @CustomerCustType                                         CustTypeType
   , @CustomerTermsCode                                        TermsCodeType
   , @CustomerShipCode                                         ShipCodeType
   , @CustomerSlsman                                           SlsmanType
   , @CustomerStateCycle                                       StatementCycleType
   , @CustomerFinChg                                           ListYesNoType
   , @CustomerLastInv                                          DateType
   , @CustomerLastPaid                                         DateType
   , @CustomerSalesYtd                                         AmountType
   , @CustomerSalesLstYr                                       AmountType
   , @CustomerDiscYtd                                          AmountType
   , @CustomerDiscLstYr                                        AmountType
   , @CustomerLastFinChg                                       DateType
   , @CustomerSalesPtd                                         AmountType
   , @CustomerCalcDate                                         DateType
   , @CustomerNumPeriods                                       PeriodsAveragedType
   , @CustomerAvgDaysOs                                        DaysOSType
   , @CustomerNumInvoices                                      InvoicesType
   , @CustomerHistDaysOs                                       TotalDaysOSType
   , @CustomerLargDaysOs                                       TotalDaysOSType
   , @CustomerLastDaysOs                                       DaysOSType
   , @CustomerAvgBalOs                                         AmountType
   , @CustomerLargeBalOs                                       AmountType
   , @CustomerLastBalOs                                        AmountType
   , @CustomerWhse                                             WhseType
   , @CustomerCharfld1                                         UserCharFldType
   , @CustomerCharfld2                                         UserCharFldType
   , @CustomerCharfld3                                         UserCharFldType
   , @CustomerDecifld1                                         UserDeciFldType
   , @CustomerDecifld2                                         UserDeciFldType
   , @CustomerDecifld3                                         UserDeciFldType
   , @CustomerLogifld                                          UserLogiFldType
   , @CustomerDatefld                                          UserDateFldType
   , @CustomerTaxRegNum1                                       TaxRegNumType
   , @CustomerBankCode                                         BankCodeType
   , @CustomerTaxRegNum2                                       TaxRegNumType
   , @CustomerPayType                                          CustPayTypeType
   , @CustomerEdiCust                                          ListYesNoType
   , @CustomerBranchId                                         BranchIdType
   , @CustomerTransNat                                         TransNatType
   , @CustomerDelterm                                          DeltermType
   , @CustomerProcessInd                                       ProcessIndType
   , @CustomerUseExchRate                                      ListYesNoType
   , @CustomerTaxCode1                                         TaxCodeType
   , @CustomerTaxCode2                                         TaxCodeType
   , @CustomerPricecode                                        PriceCodeType
   , @CustomerShipEarly                                        ListYesNoType
   , @CustomerShipPartial                                      ListYesNoType
   , @CustomerLangCode                                         LangCodeType
   , @CustomerEndUserType                                      EndUserTypeType
   , @CustomerShipSite                                         SiteType
   , @CustomerLcrReqd                                          ListYesNoType
   , @CustomerCustBank                                         BankCodeType
   , @CustomerDraftPrintFlag                                   ListYesNoType
   , @CustomerRcvInternalEmail                                 ListYesNoType
   , @CustomerCustomerEmailAddr                                EmailType
   , @CustomerSendCustomerEmail                                ListYesNoType
   , @CustomerApsPullUp                                        ListYesNoType
   , @CustomerDoInvoice                                        DoInvoiceType
   , @CustomerConsolidate                                      ListYesNoType
   , @CustomerInvFreq                                          InvFreqType
   , @CustomerSummarize                                        ListYesNoType
   , @CustomerNoteExistsFlag                                   FlagNyType
   , @CustomerRecordDate                                       CurrentDateType
   , @CustomerRowPointer                                       RowPointerType
   , @CustomerEinvoice                                         ListYesNoType
   , @CustomerOrderBal                                         AmountType
   , @CustomerPostedBal                                        AmountType
   , @CustomerCreatedBy                                        UsernameType
   , @CustomerUpdatedBy                                        UsernameType
   , @CustomerCreateDate                                       CurrentDateType
   , @CustomerCrmGuid                                          ExternalGUIDType
   , @CustomerInWorkflow                                       FlagNyType
   , @CustomerPrintPackInv                                     ListYesNoType
   , @CustomerOnePackInv                                       ListYesNoType
   , @CustomerInvCategory                                      InvCategoryType
   , @CustomerIncludeTaxInPrice                                ListYesNoType
   , @CustomerTransNat2                                        TransNat2Type
   , @CustomerUseRevisionPayDays                               ListYesNoType
   , @CustomerRevisionDay                                      WeekDayType
   , @CustomerRevisionDayStartTime##1                          TimeType
   , @CustomerRevisionDayStartTime##2                          TimeType
   , @CustomerRevisionDayEndTime##1                            TimeType
   , @CustomerRevisionDayEndTime##2                            TimeType
   , @CustomerPayDay                                           WeekDayType
   , @CustomerPayDayStartTime##1                               TimeType
   , @CustomerPayDayStartTime##2                               TimeType
   , @CustomerPayDayEndTime##1                                 TimeType
   , @CustomerPayDayEndTime##2                                 TimeType
   , @CustomerExportType                                       ListDirectIndirectNonExportType
   , @CustomerActiveForDataIntegration                         ListYesNoType
   , @CustomerShowInShipToDropDownList                         ListYesNoType
   , @CustomerShowInDropDownList                               ListYesNoType
   , @CustomerSicCode                                          SICCodeType
   , @CustomerNumberOfEmployees                                NumberOfEmployeesType
   , @CustomerCompanyRevenue                                   AmountType
   , @CustomerTerritoryCode                                    TerritoryCodeType
   , @CustomerSalesTeamId                                      SalesTeamIDType
   , @CustomerDaysShippedBeforeDueDateTolerance                ToleranceDaysType
   , @CustomerDaysShippedAfterDueDateTolerance                 ToleranceDaysType
   , @CustomerShippedOverOrderedQtyTolerance                   TolerancePercentType
   , @CustomerShippedUnderOrderedQtyTolerance                  TolerancePercentType
   , @CustomerDefaultShipTo                                    CustSeqType

-- Customer Address
   , @CustaddrCustNum                                          CustNumType
   , @CustaddrCustSeq                                          CustSeqType
   , @CustaddrName                                             NameType
   , @CustaddrCity                                             CityType
   , @CustaddrState                                            StateType
   , @CustaddrZip                                              PostalCodeType
   , @CustaddrCounty                                           CountyType
   , @CustaddrCountry                                          CountryType
   , @CustaddrFaxNum                                           PhoneType
   , @CustaddrTelexNum                                         PhoneType
   , @CustaddrBalMethod                                        BalMethodType
   , @CustaddrAddr##1                                          AddressType
   , @CustaddrAddr##2                                          AddressType
   , @CustaddrAddr##3                                          AddressType
   , @CustaddrAddr##4                                          AddressType
   , @CustaddrCreditHold                                       ListYesNoType
   , @CustaddrCreditHoldUser                                   UserCodeType
   , @CustaddrCreditHoldDate                                   DateType
   , @CustaddrCreditHoldReason                                 ReasonCodeType
   , @CustaddrCreditLimit                                      AmountType
   , @CustaddrCurrCode                                         CurrCodeType
   , @CustaddrCorpCust                                         CustNumType
   , @CustaddrCorpCred                                         ListYesNoType
   , @CustaddrCorpAddress                                      ListYesNoType
   , @CustaddrAmtOverInvAmt                                    AmountType
   , @CustaddrDaysOverInvDueDate                               DaysOverType
   , @CustaddrShipToEmail                                      EmailType
   , @CustaddrBillToEmail                                      EmailType
   , @CustaddrInternetUrl                                      URLType
   , @CustaddrInternalEmailAddr                                EmailType
   , @CustaddrExternalEmailAddr                                EmailType
   , @CustaddrNoteExistsFlag                                   FlagNyType
   , @CustaddrRecordDate                                       CurrentDateType
   , @CustaddrRowPointer                                       RowPointerType
   , @CustaddrCreatedBy                                        UsernameType
   , @CustaddrUpdatedBy                                        UsernameType
   , @CustaddrCreateDate                                       CurrentDateType
   , @CustaddrInWorkflow                                       FlagNyType
   , @CustaddrCarrierAccount                                   CarrierAccountType
   , @CustaddrCarrierUpchargePct                               CarrierUpchargePercentType
   , @CustaddrCarrierResidentialIndicator                      ListYesNoType
   , @CustaddrCarrierBillToTransportation                      CarrierBillToTransportationType

   , @ReasonDescription                                        DescriptionType

   , @BankHdrName                                              NameType
   , @BankHdrCurrCode                                          CurrCodeType
   , @BankHdrBankAcctNo                                        BankAccountType
   , @BankHdrBusinessIdentifierCode                            BusinessIdentifierCodeType
   , @BankHdrInternationalBankAccount                          InternationalBankAccountType

   , @BankAddrAddr##1                                          AddressType
   , @BankAddrAddr##2                                          AddressType
   , @BankAddrBranchCode                                       BranchCodeType
   , @BankAddrCity                                             CityType
   , @BankAddrZip                                              PostalCodeType
   , @BankAddrCountry                                          CountryType

SELECT @ParmsSite = site
FROM parms
WHERE parm_key = 0

SELECT @Site = site
FROM extfin_parms
WHERE parm_key = 0

SELECT @DatabaseName= app_db_name
FROM site
WHERE site.site = @Site

SELECT @ServerName = linked_server_name
FROM site_link_info
WHERE from_site = @ParmsSite
  AND to_site = @Site

SET @Today = dbo.GetSiteDate(GETDATE())
SET @CompanyCode = @Site

IF ( @CompanyCode IS NULL )
  BEGIN
   EXEC @Severity = MsgAppSp @Infobar OUTPUT,
                     'I=IsCompare0'
                   , '@extfin_parms.site'
                   , '@!NotSet'
                   , '@extfin_parms'
   RETURN @Severity
  END

IF ( @DatabaseName IS NULL )
  BEGIN
   EXEC @Severity = MsgAppSp @Infobar OUTPUT,
                     'I=IsCompare0'
                   , '@site.app_db_name'
                   , '@!NotSet'
                   , '@site'
   RETURN @Severity
  END

IF ( @ServerName IS NULL )
  BEGIN
   EXEC @Severity = MsgAppSp @Infobar OUTPUT,
                     'I=IsCompare0'
                   , '@site_link_info.linked_server_name'
                   , '@!NotSet'
                   , '@site_link_info'
   RETURN @Severity
  END

SET @DSqlVEIS_PERSONAL_ACCOUNT_Parms            = N'  @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @DSqlPUBPERSACCBASE_V_SubAccountCheck_Parms = N'  @IsIGFParentOrNewPersonalAcct TINYINT OUTPUT '
                                                + N', @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER_Parms  = N'  @VEIS_PERSONAL_ACCOUNT_REQNUMBER	NUMERIC ( 18 , 0 ) OUTPUT '
                                                + N', @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER        = N'SET @VEIS_PERSONAL_ACCOUNT_REQNUMBER = ISNULL ( ( SELECT MAX ( vpac.REQNUMBER ) '
                                                + N'FROM '
                                                --+ @ServerName
                                                --+ N'.'
                                                --+ @DatabaseName
                                                --+ N'.'
                                                --+ N'dbo'
                                                --+ N'.'
                                                + N'VEIS_PERSONAL_ACCOUNT AS vpac ) , 0 ) + 1 '

SET @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT_Parms = N'  @VEIS_PERSONAL_ACCOUNT_REQNUMBER  NUMERIC ( 18 , 0 ) '
                                                + N', @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT  NUMERIC ( 18 , 0 ) OUTPUT '
                                                + N', @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT = N'SELECT @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT = vpac.PK_VEIS_PERSONAL_ACCOUNT '
                                                + N'FROM '
                                                --+ @ServerName
                                                --+ N'.'
                                                --+ @DatabaseName
                                                --+ N'.'
                                                --+ N'dbo'
                                                --+ N'.'
                                                + N'VEIS_PERSONAL_ACCOUNT AS vpac '
                                                + N'WHERE vpac.REQNUMBER = @VEIS_PERSONAL_ACCOUNT_REQNUMBER '

SET @DSql_PUBPERSACCREPADV_V_GroupExist_Parms   = N''
                                                + N'  @CustNum NVARCHAR ( 7 ) '
                                                + N', @CompanyCode NVARCHAR ( 8 ) '
                                                + N', @CustaddrCorpCust NVARCHAR ( 7 ) '
                                                + N', @GroupAcctExists TINYINT OUTPUT '
                                                + N', @GroupAcctRecDefined  TINYINT OUTPUT '
                                                + N', @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @DSql_PUBPERSACCREPADV_V_GroupExist         = N''
                                                + N'; '
                                                + N'WITH CompanyCorpCust ( CompanyCode , PersAccAssocCode , CustCode ) '
                                                + N'AS ( SELECT '
                                                + N'             ppara.COMPANYCODE '
                                                + N'           , ppara.PERSACCASSOCODE '
                                                + N'           , ppara.CODE '
                                                + N'     FROM  '
                                                + QUOTENAME(@ServerName)
                                                + N'.'
                                                + QUOTENAME(@DatabaseName)
                                                + N'.'
                                                + N'varial'
                                                + N'.'
                                                + N' PUBPERSACCREPADV_V AS ppara '
                                                + N'     WHERE 1 = 1 '
                                                + N'       AND ppara.ISCUSTOMER = NCHAR(84) '   -- N'T'
                                                + N'       AND ppara.PERSACCASSOCODE = ppara.CODE '
                                                + N'       AND ppara.COMPANYCODE = @CompanyCode '
                                                + N'   ) '
                                                + N'SELECT '
                                                + N'        @GroupAcctExists      = CASE '
                                                + N'                                 WHEN ccc.PersAccAssocCode = LTRIM ( RTRIM ( @CustaddrCorpCust ) ) '
                                                + N'THEN 1 '
                                                + N'ELSE 0 '
                                                + N'END '
                                                + N'      , @GroupAcctRecDefined  = CASE '
                                                + N'                                 WHEN ppara.PERSACCASSOCODE = LTRIM ( RTRIM ( @CustaddrCorpCust ) ) '
                                                + N'THEN 1 '
                                                + N'ELSE 0 '
                                                + N'END '
                                                + N'FROM  CompanyCorpCust AS ccc '
                                                + N'LEFT OUTER JOIN '
                                                + QUOTENAME(@ServerName)
                                                + N'.'
                                                + QUOTENAME(@DatabaseName)
                                                + N'.'
                                                + N'varial'
                                                + N'.'
                                                + N'PUBPERSACCREPADV_V AS ppara '
                                                + N'   ON ccc.CompanyCode = ppara.COMPANYCODE '
                                                + N'  AND ccc.PersAccAssocCode = ppara.PERSACCASSOCODE '
                                                + N'  AND ppara.CODE = LTRIM ( RTRIM ( @CustNum ) ) '
                                                + N'WHERE 1 = 1 '
                                                + N'  AND ccc.CompanyCode = @CompanyCode '
                                                + N'  AND ccc.PersAccAssocCode = LTRIM ( RTRIM ( @CustaddrCorpCust ) ) '

SET @DSqlVEIS_PERSONAL_ACCOUNT_CORP_Parms       = N''
                                                + N'  @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT  NUMERIC ( 18 , 0 ) '
                                                + N', @CustNum NVARCHAR ( 7 ) '
                                                + N', @CustaddrCorpCust NVARCHAR ( 7 ) '
                                                + N', @Severity INT OUTPUT '
                                                + N', @Infobar NVARCHAR ( 2800 ) OUTPUT '

SET @XactAbortSub001                         = N''
                                             + N' SET XACT_ABORT ON '

SET @TryCatchSub001                          = N''
                                             + N'   BEGIN TRY '

SET @TryCatchSub002                          = N''
                                             + N'   END TRY '
                                             + N'   BEGIN CATCH '
                                             + N'   SELECT '
                                             + N'     @Severity = ERROR_SEVERITY() '
                                             + N'     , @Infobar = ISNULL ( ERROR_PROCEDURE ( ) + N'', '' , N'''' ) '
                                             + N'       + N''Error Number: '' + CAST ( ERROR_NUMBER() AS NVARCHAR ) '
                                             + N'       + N'', Error Message: '' + ERROR_MESSAGE()'
                                             + N'   END CATCH '

SET @Severity = 0

SELECT
     @ARParmsARAcct = arparms.ar_acct
   , @ARParmsARAcctUnit1 = arparms.ar_acct_unit1
   , @ARParmsARAcctUnit2 = arparms.ar_acct_unit2
   , @ARParmsARAcctUnit3 = arparms.ar_acct_unit3
   , @ARParmsARAcctUnit4 = arparms.ar_acct_unit4
   , @ARParmsSalesAcct = arparms.sales_acct
   , @ARParmsSalesAcctUnit1 = arparms.sales_acct_unit1
   , @ARParmsSalesAcctUnit2 = arparms.sales_acct_unit2
   , @ARParmsSalesAcctUnit3 = arparms.sales_acct_unit3
   , @ARParmsSalesAcctUnit4 = arparms.sales_acct_unit4
   , @ARParmsMiscAcct = arparms.misc_acct
   --, @ARParmsMiscAcctUnit1 = arparms.misc_acct_unit1
   --, @ARParmsMiscAcctUnit2 = arparms.misc_acct_unit2
   --, @ARParmsMiscAcctUnit3 = arparms.misc_acct_unit3
   --, @ARParmsMiscAcctUnit4 = arparms.misc_acct_unit4
   --, @ARParmsProgAcct = arparms.prog_acct
   --, @ARParmsProgAcctUnit1 = arparms.prog_acct_unit1
   --, @ARParmsProgAcctUnit2 = arparms.prog_acct_unit2
   --, @ARParmsProgAcctUnit3 = arparms.prog_acct_unit3
   --, @ARParmsProgAcctUnit4 = arparms.prog_acct_unit4
   --, @ARParmsSalesDiscAcct = arparms.sales_disc_acct
   --, @ARParmsSalesDiscAcctUnit1 = arparms.sales_disc_acct_unit1
   --, @ARParmsSalesDiscAcctUnit2 = arparms.sales_disc_acct_unit2
   --, @ARParmsSalesDiscAcctUnit3 = arparms.sales_disc_acct_unit3
   --, @ARParmsSalesDiscAcctUnit4 = arparms.sales_disc_acct_unit4
   --, @ARParmsProjAcct = arparms.proj_acct
   --, @ARParmsProjAcctUnit1 = arparms.proj_acct_unit1
   --, @ARParmsProjAcctUnit2 = arparms.proj_acct_unit2
   --, @ARParmsProjAcctUnit3 = arparms.proj_acct_unit3
   --, @ARParmsProjAcctUnit4 = arparms.proj_acct_unit4
   --, @ARParmsDraftReceivableAcct = arparms.draft_receivable_acct
   --, @ARParmsDraftReceivableAcctUnit1 = arparms.draft_receivable_acct_unit1
   --, @ARParmsDraftReceivableAcctUnit2 = arparms.draft_receivable_acct_unit2
   --, @ARParmsDraftReceivableAcctUnit3 = arparms.draft_receivable_acct_unit3
   --, @ARParmsDraftReceivableAcctUnit4 = arparms.draft_receivable_acct_unit4

FROM arparms
WHERE arparms_key = 0

SET @CustNum = ISNULL(dbo.ExpandKyByType('CustNumType',@CustNum), '')

SELECT
     @CustomerCustNum = customer.cust_num
   , @CustomerCustSeq = customer.cust_seq
   , @CustomerContact##1 = customer.contact##1
   , @CustomerContact##2 = customer.contact##2
   , @CustomerContact##3 = customer.contact##3
   , @CustomerPhone##1 = customer.phone##1
   , @CustomerPhone##2 = customer.phone##2
   , @CustomerPhone##3 = customer.phone##3
   , @CustomerCustType = customer.cust_type
   , @CustomerTermsCode = customer.terms_code
   , @CustomerShipCode = customer.ship_code
   , @CustomerSlsman = customer.slsman
   , @CustomerStateCycle = customer.state_cycle
   , @CustomerFinChg = customer.fin_chg
   , @CustomerLastInv = customer.last_inv
   , @CustomerLastPaid = customer.last_paid
   , @CustomerSalesYtd = customer.sales_ytd
   , @CustomerSalesLstYr = customer.sales_lst_yr
   , @CustomerDiscYtd = customer.disc_ytd
   , @CustomerDiscLstYr = customer.disc_lst_yr
   , @CustomerLastFinChg = customer.last_fin_chg
   , @CustomerSalesPtd = customer.sales_ptd
   , @CustomerCalcDate = customer.calc_date
   , @CustomerNumPeriods = customer.num_periods
   , @CustomerAvgDaysOs = customer.avg_days_os
   , @CustomerNumInvoices = customer.num_invoices
   , @CustomerHistDaysOs = customer.hist_days_os
   , @CustomerLargDaysOs = customer.larg_days_os
   , @CustomerLastDaysOs = customer.last_days_os
   , @CustomerAvgBalOs = customer.avg_bal_os
   , @CustomerLargeBalOs = customer.large_bal_os
   , @CustomerLastBalOs = customer.last_bal_os
   , @CustomerWhse = customer.whse
   , @CustomerCharfld1 = customer.charfld1
   , @CustomerCharfld2 = customer.charfld2
   , @CustomerCharfld3 = customer.charfld3
   , @CustomerDecifld1 = customer.decifld1
   , @CustomerDecifld2 = customer.decifld2
   , @CustomerDecifld3 = customer.decifld3
   , @CustomerLogifld = customer.logifld
   , @CustomerDatefld = customer.datefld
   , @CustomerTaxRegNum1 = customer.tax_reg_num1
   , @CustomerBankCode = customer.bank_code
   , @CustomerTaxRegNum2 = customer.tax_reg_num2
   , @CustomerPayType = customer.pay_type
   , @CustomerEdiCust = customer.edi_cust
   , @CustomerBranchId = customer.branch_id
   , @CustomerTransNat = customer.trans_nat
   , @CustomerDelterm = customer.delterm
   , @CustomerProcessInd = customer.process_ind
   , @CustomerUseExchRate = customer.use_exch_rate
   , @CustomerTaxCode1 = customer.tax_code1
   , @CustomerTaxCode2 = customer.tax_code2
   , @CustomerPricecode = customer.pricecode
   , @CustomerShipEarly = customer.ship_early
   , @CustomerShipPartial = customer.ship_partial
   , @CustomerLangCode = customer.lang_code
   , @CustomerEndUserType = customer.end_user_type
   , @CustomerShipSite = customer.ship_site
   , @CustomerLcrReqd = customer.lcr_reqd
   , @CustomerCustBank = customer.cust_bank
   , @CustomerDraftPrintFlag = customer.draft_print_flag
   , @CustomerRcvInternalEmail = customer.rcv_internal_email
   , @CustomerCustomerEmailAddr = customer.customer_email_addr
   , @CustomerSendCustomerEmail = customer.send_customer_email
   , @CustomerApsPullUp = customer.aps_pull_up
   , @CustomerDoInvoice = customer.do_invoice
   , @CustomerConsolidate = customer.consolidate
   , @CustomerInvFreq = customer.inv_freq
   , @CustomerSummarize = customer.summarize
   , @CustomerNoteExistsFlag = customer.NoteExistsFlag
   , @CustomerRecordDate = customer.RecordDate
   , @CustomerRowPointer = customer.RowPointer
   , @CustomerEinvoice = customer.einvoice
   , @CustomerOrderBal = customer.order_bal
   , @CustomerPostedBal = customer.posted_bal
   , @CustomerCreatedBy = customer.CreatedBy
   , @CustomerUpdatedBy = customer.UpdatedBy
   , @CustomerCreateDate = customer.CreateDate
   , @CustomerCrmGuid = customer.crm_guid
   , @CustomerInWorkflow = customer.InWorkflow
   , @CustomerPrintPackInv = customer.print_pack_inv
   , @CustomerOnePackInv = customer.one_pack_inv
   , @CustomerInvCategory = customer.inv_category
   , @CustomerIncludeTaxInPrice = customer.include_tax_in_price
   , @CustomerTransNat2 = customer.trans_nat_2
   , @CustomerUseRevisionPayDays = customer.use_revision_pay_days
   , @CustomerRevisionDay = customer.revision_day
   , @CustomerRevisionDayStartTime##1 = customer.revision_day_start_time##1
   , @CustomerRevisionDayStartTime##2 = customer.revision_day_start_time##2
   , @CustomerRevisionDayEndTime##1 = customer.revision_day_end_time##1
   , @CustomerRevisionDayEndTime##2 = customer.revision_day_end_time##2
   , @CustomerPayDay = customer.pay_day
   , @CustomerPayDayStartTime##1 = customer.pay_day_start_time##1
   , @CustomerPayDayStartTime##2 = customer.pay_day_start_time##2
   , @CustomerPayDayEndTime##1 = customer.pay_day_end_time##1
   , @CustomerPayDayEndTime##2 = customer.pay_day_end_time##2
   , @CustomerExportType = customer.export_type
   , @CustomerActiveForDataIntegration = customer.active_for_data_integration
   , @CustomerShowInShipToDropDownList = customer.show_in_ship_to_drop_down_list
   , @CustomerShowInDropDownList = customer.show_in_drop_down_list
   , @CustomerSicCode = customer.sic_code
   , @CustomerNumberOfEmployees = customer.number_of_employees
   , @CustomerCompanyRevenue = customer.company_revenue
   , @CustomerTerritoryCode = customer.territory_code
   , @CustomerSalesTeamId = customer.sales_team_id
   , @CustomerDaysShippedBeforeDueDateTolerance = customer.days_shipped_before_due_date_tolerance
   , @CustomerDaysShippedAfterDueDateTolerance = customer.days_shipped_after_due_date_tolerance
   , @CustomerShippedOverOrderedQtyTolerance = customer.shipped_over_ordered_qty_tolerance
   , @CustomerShippedUnderOrderedQtyTolerance = customer.shipped_under_ordered_qty_tolerance
   , @CustomerDefaultShipTo = customer.default_ship_to

   , @CustaddrCustNum = custaddr.cust_num
   , @CustaddrCustSeq = custaddr.cust_seq
   , @CustaddrName = custaddr.name  -- This may be required to be not nullable for VWE
   , @CustaddrCity = custaddr.city  -- This may be required to be not nullable for VWE
   , @CustaddrState = custaddr.state
   , @CustaddrZip = custaddr.zip
   , @CustaddrCounty = custaddr.county
   , @CustaddrCountry = custaddr.country
   , @CustaddrFaxNum = custaddr.fax_num
   , @CustaddrTelexNum = custaddr.telex_num
   , @CustaddrBalMethod = custaddr.bal_method
   , @CustaddrAddr##1 = custaddr.addr##1
   , @CustaddrAddr##2 = custaddr.addr##2
   , @CustaddrAddr##3 = custaddr.addr##3
   , @CustaddrAddr##4 = custaddr.addr##4
   , @CustaddrCreditHold = custaddr.credit_hold
   , @CustaddrCreditHoldUser = custaddr.credit_hold_user
   , @CustaddrCreditHoldDate = custaddr.credit_hold_date
   , @CustaddrCreditHoldReason = custaddr.credit_hold_reason
   , @CustaddrCreditLimit = custaddr.credit_limit
   , @CustaddrCurrCode = custaddr.curr_code
   , @CustaddrCorpCust = custaddr.corp_cust
   , @CustaddrCorpCred = custaddr.corp_cred
   , @CustaddrCorpAddress = custaddr.corp_address
   , @CustaddrAmtOverInvAmt = custaddr.amt_over_inv_amt
   , @CustaddrDaysOverInvDueDate = custaddr.days_over_inv_due_date
   , @CustaddrShipToEmail = custaddr.ship_to_email
   , @CustaddrBillToEmail = custaddr.bill_to_email
   , @CustaddrInternetUrl = custaddr.internet_url
   , @CustaddrInternalEmailAddr = custaddr.internal_email_addr
   , @CustaddrExternalEmailAddr = custaddr.external_email_addr
   , @CustaddrNoteExistsFlag = custaddr.NoteExistsFlag
   , @CustaddrRecordDate = custaddr.RecordDate
   , @CustaddrRowPointer = custaddr.RowPointer
   , @CustaddrCreatedBy = custaddr.CreatedBy
   , @CustaddrUpdatedBy = custaddr.UpdatedBy
   , @CustaddrCreateDate = custaddr.CreateDate
   , @CustaddrInWorkflow = custaddr.InWorkflow
   , @CustaddrCarrierAccount = custaddr.carrier_account
   , @CustaddrCarrierUpchargePct = custaddr.carrier_upcharge_pct
   , @CustaddrCarrierResidentialIndicator = custaddr.carrier_residential_indicator
   , @CustaddrCarrierBillToTransportation = custaddr.carrier_bill_to_transportation

   , @EndTypeARAcct = endtype.ar_acct
   , @EndTypeARAcctUnit1 = endtype.ar_acct_unit1
   , @EndTypeARAcctUnit2 = endtype.ar_acct_unit2
   , @EndTypeARAcctUnit3 = endtype.ar_acct_unit3
   , @EndTypeARAcctUnit4 = endtype.ar_acct_unit4
   , @EndTypeSalesAcct = endtype.sales_acct
   , @EndTypeSalesAcctUnit1 = endtype.sales_acct_unit1
   , @EndTypeSalesAcctUnit2 = endtype.sales_acct_unit2
   , @EndTypeSalesAcctUnit3 = endtype.sales_acct_unit3
   , @EndTypeSalesAcctUnit4 = endtype.sales_acct_unit4

   , @ReasonDescription = reason.description

   , @BankHdrName = bank_hdr.name
   , @BankHdrCurrCode = bank_hdr.curr_code
   , @BankHdrBankAcctNo = customer.bank_acct_no
   , @BankHdrBusinessIdentifierCode = bank_hdr.business_identifier_code
   , @BankHdrInternationalBankAccount = customer.international_bank_account

   , @BankAddrAddr##1 = bank_addr.addr##1
   , @BankAddrAddr##2 = bank_addr.addr##2
   , @BankAddrBranchCode = bank_addr.branch_code
   , @BankAddrCity = bank_addr.city
   , @BankAddrZip = bank_addr.zip
   , @BankAddrCountry = bank_addr.country

   FROM customer
   INNER JOIN custaddr
      ON customer.cust_num = custaddr.cust_num
     AND customer.cust_seq = custaddr.cust_seq
   LEFT OUTER JOIN endtype
      ON customer.end_user_type = endtype.end_user_type
   LEFT OUTER JOIN reason
      ON reason.reason_class = 'CRED HOLD'
     AND custaddr.credit_hold_reason = reason.reason_code
   LEFT OUTER JOIN bank_hdr
      ON customer.cust_bank = bank_hdr.bank_code
   LEFT OUTER JOIN bank_addr
      ON bank_hdr.bank_code = bank_addr.bank_code

WHERE 1 = 1
  AND customer.cust_num = @CustNum
  AND customer.cust_seq = 0

-- Determine if @CustNum is also @CorpCustNum
IF EXISTS ( SELECT 1 FROM custaddr WHERE custaddr.corp_cust = @CustNum )
   SET @IsHeadAccount = 1

IF @CustomerRowPointer IS NOT NULL
  BEGIN
   -- VEIS_PERSONAL_ACCOUNT
   -- Technical Fields Database
   SET @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT     = NULL
   SET @VEIS_PERSONAL_ACCOUNT_ENTITYDATE                   = @TFD_ENTITYDATE
   SET @VEIS_PERSONAL_ACCOUNT_ENTITYSTATE                  = @TFD_ENTITYSTATE

   -- Professional Fields Database
   SET @VEIS_PERSONAL_ACCOUNT_COMPANYID                    = @CompanyCode
   SET @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE                  = @PATE_CUSTOMER  -- Type of Personal Account is a customer
   SET @VEIS_PERSONAL_ACCOUNT_PRESETID                     = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CODE                         = LTRIM ( RTRIM ( @CustomerCustNum ) )
   SET @VEIS_PERSONAL_ACCOUNT_SHORTNAME                    = LTRIM ( RTRIM ( @CustomerCustNum ) )  -- same as CODE
   -- SET @VEIS_PERSONAL_ACCOUNT_SUNDRYACCOUNT                = -- let default constraint populate value in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PA_NAME                      = @CustaddrName
   SET @VEIS_PERSONAL_ACCOUNT_ADDRESS                      = ISNULL ( @CustaddrAddr##1 , '' )
                                                           + ISNULL ( CHAR ( 13 ) + CHAR ( 10 ) + @CustaddrAddr##2 , '' )     -- VEIS permits only two lines separated by \r \n.
   SET @VEIS_PERSONAL_ACCOUNT_POSTCODE                     = @CustaddrZip
   SET @VEIS_PERSONAL_ACCOUNT_TOWN                         = ISNULL ( @CustaddrCity , @MaintainAlphanumericDefault ) -- Mandatory Field
   SET @VEIS_PERSONAL_ACCOUNT_COUNTY                       = @CustaddrCounty
   SET @VEIS_PERSONAL_ACCOUNT_COUNTRY                      = ISNULL ( @CustaddrCountry , @MaintainAlphanumericDefault ) -- Mandatory Field
   SET @VEIS_PERSONAL_ACCOUNT_VATTAXNUMBER                 = @CustomerTaxRegNum1

   SELECT  @VEIS_PERSONAL_ACCOUNT_ECTAXNUMBER               = CASE
                                                               WHEN cou.ec_code IS NOT NULL
                                                               THEN REPLACE ( cst.tax_reg_num1 , ISNULL ( cou.ec_code , N'' ) , N'' )
                                                               ELSE @MaintainAlphanumericDefault
                                                             END
         , @VEIS_PERSONAL_ACCOUNT_BANK_ACCOUNTNUMBER        = CASE
                                                               WHEN cou.iso_country_code = N'DE'
                                                               THEN @MaintainAlphanumericDefault
                                                               ELSE ISNULL ( @BankHdrBankAcctNo , @MaintainAlphanumericDefault )
                                                              END
   FROM  custaddr AS cad
   INNER JOIN
         customer AS cst
      ON cad.cust_num = cst.cust_num
     AND cad.cust_seq = cst.cust_seq
   LEFT OUTER JOIN
         country AS cou
      ON cad.country = cou.country
   WHERE 1 = 1
     AND cad.cust_num = @CustNum
     AND cad.cust_seq = 0

   SET @VEIS_PERSONAL_ACCOUNT_LANGUAGEID                   = @CustomerLangCode -- VWE import code
   SET @VEIS_PERSONAL_ACCOUNT_PHONE                        = @CustomerPhone##1 -- Bill-To
   SET @VEIS_PERSONAL_ACCOUNT_FAX                          = @CustaddrFaxNum
   SET @VEIS_PERSONAL_ACCOUNT_EMAIL                        = @CustaddrExternalEmailAddr
   SET @VEIS_PERSONAL_ACCOUNT_HOMEPAGE                     = @CustaddrInternetUrl
   SET @VEIS_PERSONAL_ACCOUNT_COMPANYNUMBER                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOX                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOXPOSTCODE        = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RITENUTATYPE                 = @RTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CODICEFISCALEORGIURIDICE     = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_TAXOFFICE                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CODICECAUSALE                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_TAXREGISTER                  = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RITENUTATAXCODE              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BIRTHDATE                    = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BIRTHPLACE                   = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BIRTHCOUNTRY                 = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_FIRSTNAME                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_SURNAME                      = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_SEX                          = @SE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTROLACCOUNTID             = CASE
                                                                WHEN @EndTypeARAcct IS NULL
                                                                THEN @ARParmsARAcct
                                                                ELSE @EndTypeARAcct
                                                             END
   SET @VEIS_PERSONAL_ACCOUNT_DEFAULTACCOUNTID             = CASE
                                                                WHEN @EndTypeSalesAcct IS NULL
                                                                THEN @ARParmsSalesAcct
                                                                ELSE @EndTypeSalesAcct
                                                             END
   SET @VEIS_PERSONAL_ACCOUNT_CURRENCYID                   = @CustaddrCurrCode -- VWE import code
   SET @VEIS_PERSONAL_ACCOUNT_TERMSOFPAYMENTID             = @CustomerTermsCode -- VWE import code
   SET @VEIS_PERSONAL_ACCOUNT_TAXCODEID                    = @CustomerTaxCode1 -- VWE import code
   SET @VEIS_PERSONAL_ACCOUNT_MODEOFPAYMENT                = CASE
                                                                WHEN @CustomerPayType = 'C' THEN @PTE_CHEQUE
                                                                WHEN @CustomerPayType = 'D' THEN @PTE_BILLOFEXCHANGE
                                                                WHEN @CustomerPayType = 'W' THEN @PTE_UNKNOWN
                                                                ELSE @PTE_DEFAULT
                                                             END
   SET @VEIS_PERSONAL_ACCOUNT_AC0                          = ISNULL ( @ParmsSite , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_AC1                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC2                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC3                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC4                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC5                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC6                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC7                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC8                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_AC9                          = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OWNNUMBERATPARTNER           = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CTRLACCDOWNPAYMENTID         = @ARParmsProgAcct
   SET @VEIS_PERSONAL_ACCOUNT_REPRESENTATIVEID             = @CustomerSlsman -- Sales Representative
   SET @VEIS_PERSONAL_ACCOUNT_ADVISORID                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_ISSTOP                       = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_STOPNOTE                     = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_COSTCENTREID                 = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_NEGEXCHANGERATE              = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_TERMOFPAYMBILLID             = CASE
                                                                WHEN @VEIS_PERSONAL_ACCOUNT_MODEOFPAYMENT = @PTE_BILLOFEXCHANGE THEN @CustomerTermsCode -- VWE import code
                                                                ELSE @MaintainAlphanumericDefault
                                                             END
   SET @VEIS_PERSONAL_ACCOUNT_TOLERANCEFORBILLRISK         = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_OIAC0                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC1                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC2                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC3                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC4                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC5                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC6                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC7                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC8                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_OIAC9                        = @MaintainAlphanumericDefault -- TBD
   SET @VEIS_PERSONAL_ACCOUNT_WITHHOLD                     = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_WITHHOLDNOTE                 = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_EXCLUDEFROMCENTRALPAYMENTS   = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMDAY          = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMMONTH        = @ME_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILDAY         = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILMONTH       = @ME_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_TERMSOFPAYMENTID = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_FACTORING                    = @FTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_FACTORINGNOTE                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CESSIONBANKID                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_DATE               = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_TYPE               = @RETE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_INFORMANT          = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_SHORTADDRESS       = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_PHONE              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REFERENCE_DETAILS            = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DUNNING_GROUPID              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REMINDER_BYPHONE             = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REMINDER_BYLETTER            = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REMINDER_BYFAX               = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_REMINDER_BYEMAIL             = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOP                = CASE
                                                                WHEN @CustaddrCreditHold = 1 THEN 'T'
                                                                ELSE 'F'
                                                             END
   SET @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOPNOTE            = @ReasonDescription
   SET @VEIS_PERSONAL_ACCOUNT_CREDITINSURANCECODE          = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_INSURANCE_CONTRACTNUMBER     = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_INSURANCE_RETENTION          = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_INSURANCE_CUSTOMERNUMBER     = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_FROMDATE               = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_UNTILDATE              = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_TYPE                   = @CLTE_CREDIT_LIMIT
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_AMOUNT                 = @MaintainNumericDefault -- Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_MAXDAYOVERDUE          = @MaintainNumericDefault -- Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_STATUS                 = @LSE_DEFAULT
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_REFUSALREASONID        = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_LIMIT_NOTE                   = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DELIVERY_REFUSAL_REASONID    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_SHORTADDRESS       = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_NAME               = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_ADDRESS            = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTCODE           = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTY             = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTRY            = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_TOWN               = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTOFFICEBOX      = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_POBPOSTCODE        = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_PHONE              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_FAX                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_EMAIL              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_LANGUAGEID         = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_COMPANYNUMBER      = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDFROM          = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDUNTIL         = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_EVENTTYPE          = @REE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_DISPATCHTYPE       = @DTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NAME               = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_ADDRESS            = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NOTE               = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_CONTACT                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_PAYMENTTYPE            = @PTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_CURRENCYID             = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_OPENAMOUNT             = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDAMOUNT         = @MaintainNumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDDATE           = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_TASKREMINDER           = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_STATE                  = @APTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_AGREE_NOTE                   = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_NAME                 = @CustomerContact##3
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_TOWN                 = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_PHONE                = @CustomerPhone##3
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_FAX                  = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_EMAIL                = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_PROFESSION           = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_LANGUAGEID           = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_NOTE                 = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_CONTACT_REMINDERDEFAULT      = NULL -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_NAME                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_SUBJECT                 = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_TEXT                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_PRIO                    = @PRTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_NEXTPRIO                = @PRTE_DEFAULT -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_NOTE_DATENEXTCONTACT         = @MaintainDateDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_PAYMEXPENSEARRANGEMENT       = @EATE_DEFAULT -- N/A for customers
   SET @VEIS_PERSONAL_ACCOUNT_PAYMDECLARATIONTYPE          = @DETE_DEFAULT -- N/A for customers
   SET @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYSHORTNAME         = @MaintainAlphanumericDefault -- N/A for customers
   SET @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYCODE              = @MaintainAlphanumericDefault -- N/A for customers
   SET @VEIS_PERSONAL_ACCOUNT_PAYMDETAILSCODE              = @MaintainAlphanumericDefault -- N/A for customers
   SET @VEIS_PERSONAL_ACCOUNT_PAYMDETAILS                  = @MaintainAlphanumericDefault -- N/A for customers

   SET @VEIS_PERSONAL_ACCOUNT_BANKSORTINGCODE              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE    -- SyteLine does not provide Bank Sort Code
   SET @VEIS_PERSONAL_ACCOUNT_OTHERREFERENCE               = ISNULL ( @BankHdrInternationalBankAccount , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_BANK_NAME                    = ISNULL ( @BankHdrName , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_BANK_BRANCH                  = ISNULL ( @BankAddrBranchCode , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_BANK_ADDRESS                 = ISNULL (
                                                            ( ISNULL ( @BankAddrAddr##1 , '' )
                                                            + ISNULL ( CHAR ( 13 ) + CHAR ( 10 ) + @BankAddrAddr##2 , '' )     -- VEIS permits only two lines separated by \r \n.
                                                            ) , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_BANK_POSTCODE                = ISNULL ( @BankAddrZip , @MaintainAlphanumericDefault )  -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BANK_TOWN                    = ISNULL ( @BankAddrCity , @MaintainAlphanumericDefault ) -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BANK_COUNTY                  = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BANK_COUNTRY                 = ISNULL ( @BankAddrCountry , @MaintainAlphanumericDefault ) -- Not Used/Maintained in VWE
--   SET @VEIS_PERSONAL_ACCOUNT_BANK_ACCOUNTNUMBER           = Determined with Country Code, see above.
   SET @VEIS_PERSONAL_ACCOUNT_DEVIATING_OWNER              = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_SWIFT                        = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BANK_INTERNATIONALCODE       = ISNULL ( @BankHdrBusinessIdentifierCode , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_POSTCHEQUEACCOUNT            = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_ABANUMBER                    = @MaintainAlphanumericDefault -- Not Used/Maintained in VWE
   SET @VEIS_PERSONAL_ACCOUNT_BANK_CURRENCYID              = ISNULL ( @BankHdrCurrCode , @MaintainAlphanumericDefault )
   SET @VEIS_PERSONAL_ACCOUNT_BANK_DEFAULT                 = 'T'

   -- Determine setting of Sub-Account Flag
   IF NOT EXISTS ( SELECT 1 FROM  VEIS_PERSONAL_ACCOUNT AS vpa
                     WHERE 1 = 1
                       AND vpa.ENTITYSTATE = 0
                       AND vpa.COMPANYID = @VEIS_PERSONAL_ACCOUNT_COMPANYID
                       AND vpa.ACCOUNTTYPE = @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE
                       AND vpa.CODE = @VEIS_PERSONAL_ACCOUNT_CODE
                       AND vpa.CONTROLACCOUNTID != @VEIS_PERSONAL_ACCOUNT_CONTROLACCOUNTID
                 )
      SET @CustNumNotExistsUnprocessedInVEIS = 1

   SET @DSqlPUBPERSACCBASE_V_SubAccountCheck                = N''
                                                            + N'IF ( '
                                                            -- No match on CODE -- > Not SubAccount
                                                            + N'( NOT EXISTS '
                                                            + N' ( SELECT 1 '
                                                            + N'FROM '
                                                            + QUOTENAME(@ServerName)
                                                            + N'.'
                                                            + QUOTENAME(@DatabaseName)
                                                            + N'.'
                                                            + N'varial'
                                                            + N'.'
                                                            + N'PUBPERSACCBASE_V AS ppab '
                                                            + N'WHERE 1 = 1 '
                                                            + N'  AND ppab.COMPANYCODE '
                                                            + ISNULL ( N' = ' + NCHAR(39) +  REPLACE(CAST ( @VEIS_PERSONAL_ACCOUNT_COMPANYID AS NVARCHAR ),N'''',N'''''')  + NCHAR(39) , N'IS NULL' )
                                                            + N'AND ppab.ISCUSTOMER = '
                                                            + NCHAR(39)
                                                            + CASE @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE
                                                               WHEN 0 THEN NCHAR(84)
                                                               ELSE NCHAR(70)
                                                              END
                                                            + NCHAR(39)
                                                            + N'  AND ppab.CODE '
                                                            + ISNULL ( N' = ' + NCHAR(39) + REPLACE(CAST ( @VEIS_PERSONAL_ACCOUNT_CODE AS NVARCHAR ),N'''',N'''''')  + NCHAR(39) , N'IS NULL' )
                                                            + N' ) '  -- N' ( SELECT 1 '
                                                            + N' ) '  -- N'( NOT EXISTS '
                                                            + N'OR '
                                                            -- Exact match on CODE and NLACCCODE and OIDPARENTACCOUNT is NULL --> Not SubAccount    (Only one account exists and this is the parent)
                                                            + N'( EXISTS '
                                                            + N' ( SELECT 1 '
                                                            + N'FROM '
                                                            + QUOTENAME(@ServerName)
                                                            + N'.'
                                                            + QUOTENAME(@DatabaseName)
                                                            + N'.'
                                                            + N'varial'
                                                            + N'.'
                                                            + N'PUBPERSACCBASE_V AS ppab '
                                                            + N'WHERE 1 = 1 '
                                                            + N'  AND ppab.COMPANYCODE '
                                                            + ISNULL ( N' = ' + NCHAR(39) + REPLACE(CAST ( @VEIS_PERSONAL_ACCOUNT_COMPANYID AS NVARCHAR ),N'''',N'''''') + NCHAR(39) , N'IS NULL' )
                                                            + N'AND ppab.ISCUSTOMER = '
                                                            + NCHAR(39)
                                                            + CASE @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE
                                                               WHEN 0 THEN NCHAR(84)
                                                               ELSE NCHAR(70)
                                                              END
                                                            + NCHAR(39)
                                                            + N'  AND ppab.CODE '
                                                            + ISNULL ( N' = ' + NCHAR(39) + REPLACE(CAST ( @VEIS_PERSONAL_ACCOUNT_CODE AS NVARCHAR ),N'''',N'''''') + NCHAR(39) , N'IS NULL' )
                                                            + N'  AND ppab.NLACCCODE '
                                                            + ISNULL ( N' = ' + NCHAR(39) + REPLACE(CAST ( @VEIS_PERSONAL_ACCOUNT_CONTROLACCOUNTID AS NVARCHAR ),N'''',N'''''') + NCHAR(39) , N'IS NULL' )
                                                            + N'AND ppab.OIDPARENTACCOUNT IS NULL '
                                                            + N' ) '  -- N' ( SELECT 1 '
                                                            + N' ) '  -- N'( NOT EXISTS '
                                                            + N' ) '  -- N'IF ( '
                                                            + N'SET @IsIGFParentOrNewPersonalAcct = 1 '

   SET @BigDSql = N''
                + @XactAbortSub001
                + NCHAR ( 13 )
                + @TryCatchSub001
                + NCHAR ( 13 )
                + @DSqlPUBPERSACCBASE_V_SubAccountCheck
                + NCHAR ( 13 )
                + @TryCatchSub002

   EXECUTE sp_executesql
           @statement      = @BigDSql
         , @params         = @DSqlPUBPERSACCBASE_V_SubAccountCheck_Parms
         , @IsIGFParentOrNewPersonalAcct = @IsIGFParentOrNewPersonalAcct OUTPUT
         , @Severity = @Severity OUTPUT
         , @Infobar = @Infobar OUTPUT

   IF ( ( @IsIGFParentOrNewPersonalAcct = 1 ) AND ( @CustNumNotExistsUnprocessedInVEIS = 1 ) )
      SET @IsSubAccount = 0

   SET @VEIS_PERSONAL_ACCOUNT_ISSUBACCOUNT = CASE @IsSubAccount WHEN 1 THEN 'T' ELSE 'F' END

   -- Determine next REQNUMBER
   SET @BigDSql                              = N''
                                             + @XactAbortSub001
                                             + NCHAR ( 13 )
                                             + @TryCatchSub001
                                             + NCHAR ( 13 )
                                             + @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER
                                             + NCHAR ( 13 )
                                             + @TryCatchSub002

   EXECUTE sp_executesql
        @statement                        = @BigDSql
      , @params                           = @DSqlVEIS_PERSONAL_ACCOUNT_REQNUMBER_Parms
      , @VEIS_PERSONAL_ACCOUNT_REQNUMBER  = @VEIS_PERSONAL_ACCOUNT_REQNUMBER OUTPUT
      , @Severity                         = @Severity OUTPUT
      , @Infobar                          = @Infobar OUTPUT

      IF ( @VEIS_PERSONAL_ACCOUNT_REQNUMBER IS NOT NULL )
        BEGIN
         SET @DSqlVEIS_PERSONAL_ACCOUNT_INSERT  = ''
                                                + 'INSERT INTO '
                                              --+ @ServerName
                                              --+ '.'
                                              --+ @DatabaseName
                                              --+ '.'
                                              --+ 'dbo'
                                              --+ '.'
                                                + 'VEIS_PERSONAL_ACCOUNT ( '
                                                + '  ENTITYDATE '
                                                + ', ENTITYSTATE '
                                                + ', REQNUMBER '
                                                + ', COMPANYID '
                                                + ', ACCOUNTTYPE '
                                                + ', PRESETID '
                                                + ', CODE '
                                                + ', SHORTNAME '
                                                + ', PA_NAME '
                                                + ', ADDRESS '
                                                + ', POSTCODE '
                                                + ', TOWN '
                                                + ', COUNTY '
                                                + ', COUNTRY '
                                                + ', VATTAXNUMBER '
                                                + ', ECTAXNUMBER '
                                                + ', LANGUAGEID '
                                                + ', PHONE '
                                                + ', FAX '
                                                + ', EMAIL '
                                                + ', HOMEPAGE '
                                                + ', COMPANYNUMBER '
                                                + ', POSTOFFICEBOX '
                                                + ', POSTOFFICEBOXPOSTCODE '
                                                + ', RITENUTATYPE '
                                                + ', CODICEFISCALEORGIURIDICE '
                                                + ', TAXOFFICE '
                                                + ', CODICECAUSALE '
                                                + ', TAXREGISTER '
                                                + ', RITENUTATAXCODE '
                                                + ', BIRTHDATE '
                                                + ', BIRTHPLACE '
                                                + ', BIRTHCOUNTRY '
                                                + ', FIRSTNAME '
                                                + ', SURNAME '
                                                + ', SEX '
                                                + ', CONTROLACCOUNTID '
                                                + ', DEFAULTACCOUNTID '
                                                + ', CURRENCYID '
                                                + ', TERMSOFPAYMENTID '
                                                + ', TAXCODEID '
                                                + ', MODEOFPAYMENT '
                                                + ', AC0 '
                                                + ', AC1 '
                                                + ', AC2 '
                                                + ', AC3 '
                                                + ', AC4 '
                                                + ', AC5 '
                                                + ', AC6 '
                                                + ', AC7 '
                                                + ', AC8 '
                                                + ', AC9 '
                                                + ', OWNNUMBERATPARTNER '
                                                + ', CTRLACCDOWNPAYMENTID '
                                                + ', REPRESENTATIVEID '
                                                + ', ADVISORID '
                                                + ', ISSTOP '
                                                + ', STOPNOTE '
                                                + ', COSTCENTREID '
                                                + ', NEGEXCHANGERATE '
                                                + ', TERMOFPAYMBILLID '
                                                + ', TOLERANCEFORBILLRISK '
                                                + ', OIAC0 '
                                                + ', OIAC1 '
                                                + ', OIAC2 '
                                                + ', OIAC3 '
                                                + ', OIAC4 '
                                                + ', OIAC5 '
                                                + ', OIAC6 '
                                                + ', OIAC7 '
                                                + ', OIAC8 '
                                                + ', OIAC9 '
                                                + ', WITHHOLD '
                                                + ', WITHHOLDNOTE '
                                                + ', ISSUBACCOUNT '
                                                + ', EXCLUDEFROMCENTRALPAYMENTS '
                                                + ', PAYMENTFREE_FROMDAY '
                                                + ', PAYMENTFREE_FROMMONTH '
                                                + ', PAYMENTFREE_UNTILDAY '
                                                + ', PAYMENTFREE_UNTILMONTH '
                                                + ', PAYMENTFREE_TERMSOFPAYMENTID '
                                                + ', FACTORING '
                                                + ', FACTORINGNOTE '
                                                + ', CESSIONBANKID '
                                                + ', REFERENCE_DATE '
                                                + ', REFERENCE_TYPE '
                                                + ', REFERENCE_INFORMANT '
                                                + ', REFERENCE_SHORTADDRESS '
                                                + ', REFERENCE_PHONE '
                                                + ', REFERENCE_DETAILS '
                                                + ', DUNNING_GROUPID '
                                                + ', REMINDER_BYPHONE '
                                                + ', REMINDER_BYLETTER '
                                                + ', REMINDER_BYFAX '
                                                + ', REMINDER_BYEMAIL '
                                                + ', DELIVERY_STOP '
                                                + ', DELIVERY_STOPNOTE '
                                                + ', CREDITINSURANCECODE '
                                                + ', INSURANCE_CONTRACTNUMBER '
                                                + ', INSURANCE_RETENTION '
                                                + ', INSURANCE_CUSTOMERNUMBER '
                                                + ', LIMIT_FROMDATE '
                                                + ', LIMIT_UNTILDATE '
                                                + ', LIMIT_TYPE '
                                                + ', LIMIT_AMOUNT '
                                                + ', LIMIT_MAXDAYOVERDUE '
                                                + ', LIMIT_STATUS '
                                                + ', LIMIT_REFUSALREASONID '
                                                + ', LIMIT_NOTE '
                                                + ', DELIVERY_REFUSAL_REASONID '
                                                + ', DEVIATING_SHORTADDRESS '
                                                + ', DEVIATING_NAME '
                                                + ', DEVIATING_ADDRESS '
                                                + ', DEVIATING_POSTCODE '
                                                + ', DEVIATING_COUNTY '
                                                + ', DEVIATING_COUNTRY '
                                                + ', DEVIATING_TOWN '
                                                + ', DEVIATING_POSTOFFICEBOX '
                                                + ', DEVIATING_POBPOSTCODE '
                                                + ', DEVIATING_PHONE '
                                                + ', DEVIATING_FAX '
                                                + ', DEVIATING_EMAIL '
                                                + ', DEVIATING_LANGUAGEID '
                                                + ', DEVIATING_COMPANYNUMBER '
                                                + ', RECIPIENT_VALIDFROM '
                                                + ', RECIPIENT_VALIDUNTIL '
                                                + ', RECIPIENT_EVENTTYPE '
                                                + ', RECIPIENT_DISPATCHTYPE '
                                                + ', RECIPIENT_NAME '
                                                + ', RECIPIENT_ADDRESS '
                                                + ', RECIPIENT_NOTE '
                                                + ', AGREE_CONTACT '
                                                + ', AGREE_PAYMENTTYPE '
                                                + ', AGREE_CURRENCYID '
                                                + ', AGREE_OPENAMOUNT '
                                                + ', AGREE_PROMISEDAMOUNT '
                                                + ', AGREE_PROMISEDDATE '
                                                + ', AGREE_TASKREMINDER '
                                                + ', AGREE_STATE '
                                                + ', AGREE_NOTE '
                                                + ', CONTACT_NAME '
                                                + ', CONTACT_TOWN '
                                                + ', CONTACT_PHONE '
                                                + ', CONTACT_FAX '
                                                + ', CONTACT_EMAIL '
                                                + ', CONTACT_PROFESSION '
                                                + ', CONTACT_LANGUAGEID '
                                                + ', CONTACT_NOTE '
                                                + ', CONTACT_REMINDERDEFAULT '
                                                + ', NOTE_NAME '
                                                + ', NOTE_SUBJECT '
                                                + ', NOTE_TEXT '
                                                + ', NOTE_PRIO '
                                                + ', NOTE_NEXTPRIO '
                                                + ', NOTE_DATENEXTCONTACT '
                                                + ', PAYMEXPENSEARRANGEMENT '
                                                + ', PAYMDECLARATIONTYPE '
                                                + ', PAYMCOUNTRYSHORTNAME '
                                                + ', PAYMCOUNTRYCODE '
                                                + ', PAYMDETAILSCODE '
                                                + ', PAYMDETAILS '
                                                + ', BANKSORTINGCODE '
                                                + ', OTHERREFERENCE '
                                                + ', BANK_NAME '
                                                + ', BANK_BRANCH '
                                                + ', BANK_ADDRESS '
                                                + ', BANK_POSTCODE '
                                                + ', BANK_TOWN '
                                                + ', BANK_COUNTY '
                                                + ', BANK_COUNTRY '
                                                + ', BANK_ACCOUNTNUMBER '
                                                + ', DEVIATING_OWNER '
                                                + ', SWIFT '
                                                + ', BANK_INTERNATIONALCODE '
                                                + ', POSTCHEQUEACCOUNT '
                                                + ', ABANUMBER '
                                                + ', BANK_CURRENCYID '
                                                + ', BANK_DEFAULT '
                                                + ' ) '

         SET @DSqlVEIS_PERSONAL_ACCOUNT_VALUES  = 'VALUES ( '
                                                + ISNULL (  CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_ENTITYDATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_ENTITYSTATE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_REQNUMBER AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_COMPANYID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ACCOUNTTYPE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PRESETID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_SHORTNAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PA_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_POSTCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TOWN AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_COUNTY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_COUNTRY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_VATTAXNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ECTAXNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_LANGUAGEID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PHONE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_FAX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_EMAIL AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_HOMEPAGE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_COMPANYNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_POSTOFFICEBOXPOSTCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_RITENUTATYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CODICEFISCALEORGIURIDICE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TAXOFFICE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CODICECAUSALE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TAXREGISTER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_RITENUTATAXCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_BIRTHDATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BIRTHPLACE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BIRTHCOUNTRY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_FIRSTNAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_SURNAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_SEX AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTROLACCOUNTID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEFAULTACCOUNTID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CURRENCYID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TERMSOFPAYMENTID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TAXCODEID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_MODEOFPAYMENT AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC0 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC1 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC2 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC3 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC4 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC5 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC6 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC7 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC8 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AC9 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OWNNUMBERATPARTNER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CTRLACCDOWNPAYMENTID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REPRESENTATIVEID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ADVISORID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ISSTOP AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_STOPNOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_COSTCENTREID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_NEGEXCHANGERATE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_TERMOFPAYMBILLID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_TOLERANCEFORBILLRISK AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC0 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC1 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC2 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC3 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC4 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC5 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC6 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC7 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC8 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OIAC9 AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_WITHHOLD AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_WITHHOLDNOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ISSUBACCOUNT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_EXCLUDEFROMCENTRALPAYMENTS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMDAY AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_FROMMONTH AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILDAY AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_UNTILMONTH AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMENTFREE_TERMSOFPAYMENTID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_FACTORING AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_FACTORINGNOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CESSIONBANKID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_REFERENCE_DATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_REFERENCE_TYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REFERENCE_INFORMANT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REFERENCE_SHORTADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REFERENCE_PHONE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REFERENCE_DETAILS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DUNNING_GROUPID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REMINDER_BYPHONE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REMINDER_BYLETTER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REMINDER_BYFAX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_REMINDER_BYEMAIL AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOP AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DELIVERY_STOPNOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CREDITINSURANCECODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_INSURANCE_CONTRACTNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_INSURANCE_RETENTION AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_INSURANCE_CUSTOMERNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_LIMIT_FROMDATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_LIMIT_UNTILDATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_TYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_AMOUNT AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_MAXDAYOVERDUE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_STATUS AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_REFUSALREASONID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_LIMIT_NOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DELIVERY_REFUSAL_REASONID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_SHORTADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_ADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_COUNTRY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_TOWN AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_POSTOFFICEBOX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_POBPOSTCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_PHONE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_FAX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_EMAIL AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_LANGUAGEID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_COMPANYNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDFROM , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_RECIPIENT_VALIDUNTIL , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_RECIPIENT_EVENTTYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_RECIPIENT_DISPATCHTYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_RECIPIENT_ADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_RECIPIENT_NOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_CONTACT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_PAYMENTTYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_CURRENCYID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_OPENAMOUNT AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDAMOUNT AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_AGREE_PROMISEDDATE , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_TASKREMINDER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_STATE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_AGREE_NOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_TOWN AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_PHONE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_FAX AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_EMAIL AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_PROFESSION AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_LANGUAGEID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_NOTE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_CONTACT_REMINDERDEFAULT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_NOTE_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_NOTE_SUBJECT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_NOTE_TEXT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_NOTE_PRIO AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_NOTE_NEXTPRIO AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CONVERT ( VARCHAR(10) , @VEIS_PERSONAL_ACCOUNT_NOTE_DATENEXTCONTACT , 126 ) + CHAR(39) , 'NULL' ) + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMEXPENSEARRANGEMENT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMDECLARATIONTYPE AS VARCHAR ) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYSHORTNAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMCOUNTRYCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMDETAILSCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_PAYMDETAILS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANKSORTINGCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_OTHERREFERENCE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_NAME AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_BRANCH AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_ADDRESS AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_POSTCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_TOWN AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_COUNTY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_COUNTRY AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_ACCOUNTNUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_DEVIATING_OWNER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_SWIFT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_INTERNATIONALCODE AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_POSTCHEQUEACCOUNT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_ABANUMBER AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_CURRENCYID AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ', ' + ISNULL ( CHAR(39) + CAST ( @VEIS_PERSONAL_ACCOUNT_BANK_DEFAULT AS VARCHAR ) + CHAR(39) , 'NULL' )  + ' '
                                                + ' ) '

         IF ( @Severity = 0 )
         BEGIN
            -- Insert VEIS_PERSONAL_ACCOUNT using @VEIS_PERSONAL_ACCOUNT_REQNUMBER
            SET @BigDSql = N''
                         + @XactAbortSub001
                         + NCHAR ( 13 )
                         + @TryCatchSub001
                         + NCHAR ( 13 )
                         + @DSqlVEIS_PERSONAL_ACCOUNT_INSERT

            SET @BigDSql_2 = @DSqlVEIS_PERSONAL_ACCOUNT_VALUES
                           + NCHAR ( 13 )
                           + @TryCatchSub002

            SET @BigDSql = @BigDSql + @BigDSql_2

            EXECUTE sp_executesql
                    @statement      = @BigDSql
                  , @params         = @DSqlVEIS_PERSONAL_ACCOUNT_Parms
                  , @Severity       = @Severity OUTPUT
                  , @Infobar        = @Infobar OUTPUT

         END -- IF ( @Severity = 0 )

         --Need to handle corporate customer
         IF ( ( @CustaddrCorpCust IS NOT NULL ) OR ( @IsHeadAccount = 1 ) )
           BEGIN
               -- Test for existence of VEIS_PERSONAL_ACCOUNT where CODE matches @CustaddrCorpCust
            SET @BigDSql = N''
                         + @XactAbortSub001
                         + NCHAR ( 13 )
                         + @TryCatchSub001
                         + NCHAR ( 13 )
                         + @DSql_PUBPERSACCREPADV_V_GroupExist
                         + NCHAR ( 13 )
                         + @TryCatchSub002

            EXECUTE sp_executesql
                    @statement      = @BigDSql
                  , @params         = @DSql_PUBPERSACCREPADV_V_GroupExist_Parms
                  , @CustNum = @CustNum
                  , @CompanyCode = @CompanyCode
                  , @CustaddrCorpCust = @CustaddrCorpCust
                  , @GroupAcctExists = @GroupAcctExists OUTPUT
                  , @GroupAcctRecDefined = @GroupAcctRecDefined OUTPUT
                  , @Severity = @Severity OUTPUT
                  , @Infobar = @Infobar OUTPUT

            IF ( @Severity = 0 )
              BEGIN
               -- Get PK_VEIS_PERSONAL_ACCOUNT
               SET @BigDSql = N''
                            + @XactAbortSub001
                            + NCHAR ( 13 )
                            + @TryCatchSub001
                            + NCHAR ( 13 )
                            + @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT
                            + NCHAR ( 13 )
                            + @TryCatchSub002

               EXECUTE sp_executesql
                       @statement      = @BigDSql
                     , @params         = @DSqlVEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT_Parms
                     , @VEIS_PERSONAL_ACCOUNT_REQNUMBER = @VEIS_PERSONAL_ACCOUNT_REQNUMBER
                     , @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT = @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT OUTPUT
                     , @Severity = @Severity OUTPUT
                     , @Infobar = @Infobar OUTPUT

              END --IF ( @Severity = 0 )

            IF ( @Severity = 0 )
              BEGIN
               SET @DSqlVEIS_PERSONAL_ACCOUNT_CORP_INSERT      = N''
                                                               + N'INSERT INTO '
                                                               --+ @ServerName
                                                               --+ N'.'
                                                               --+ @DatabaseName
                                                               --+ N'.'
                                                               --+ N'varial'
                                                               --+ N'.'
                                                               + N'VEIS_PERSONAL_ACCOUNT_CORP ( '
                                                               + N'  FK_VEIS_PERSONAL_ACCOUNT '
                                                               + N', CORPORATEACCOUNTID '
                                                               + N', ISHEADACCOUNT '
                                                               + N') '

               SET @DSqlVEIS_PERSONAL_ACCOUNT_CORP_VALUES      = N'VALUES ( '
                                                               + N' @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT '
                                                               + CASE ISNULL ( @IsHeadAccount , 0 )
                                                                  WHEN 1 THEN N', @CustNum '
                                                                  ELSE N', @CustaddrCorpCust '
                                                                 END
                                                               + CASE ISNULL ( @IsHeadAccount , 0 )
                                                                  WHEN 1 THEN N', NCHAR(84) ' -- True
                                                                  ELSE N', NCHAR(70) ' -- False
                                                                 END
                                                               + N') '

               SET @BigDSql = N''
                            + @XactAbortSub001
                            + NCHAR ( 13 )
                            + @TryCatchSub001
                            + NCHAR ( 13 )
                            + @DSqlVEIS_PERSONAL_ACCOUNT_CORP_INSERT
                            + NCHAR ( 13 )
                            + @DSqlVEIS_PERSONAL_ACCOUNT_CORP_VALUES
                            + NCHAR ( 13 )
                            + @TryCatchSub002

               EXECUTE sp_executesql
                       @statement      = @BigDSql
                     , @params         = @DSqlVEIS_PERSONAL_ACCOUNT_CORP_Parms
                     , @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT = @VEIS_PERSONAL_ACCOUNT_PK_VEIS_PERSONAL_ACCOUNT
                     , @CustNum = @CustNum
                     , @CustaddrCorpCust = @CustaddrCorpCust
                     , @Severity = @Severity OUTPUT
                     , @Infobar = @Infobar OUTPUT

              END -- IF ( @Severity = 0 )

           END -- IF ( ( @CustaddrCorpCust IS NOT NULL ) OR ( @IsHeadAccount = 1 ) )

        END -- IF ( @VEIS_PERSONAL_ACCOUNT_REQNUMBER IS NOT NULL )

  END   -- IF @CustomerRowPointer IS NOT NULL

RETURN 0 --@Severity -- Want to return good code

GO