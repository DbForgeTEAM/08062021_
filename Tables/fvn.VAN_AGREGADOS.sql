﻿CREATE TABLE [fvn].[VAN_AGREGADOS] (
  [SkValor] [int] NOT NULL DEFAULT (0),
  [IDEntrada] [int] NOT NULL DEFAULT (0),
  [SECEntrada] [int] NOT NULL DEFAULT (0),
  [TRE] [smallint] NOT NULL DEFAULT (0),
  [StatusValidacion] [varchar](1) NOT NULL DEFAULT ('P'),
  [Anulado] [bit] NOT NULL DEFAULT (0),
  [IDDeclaracion] [int] NOT NULL DEFAULT (0),
  [DepositariaNR] [bit] NOT NULL DEFAULT (0),
  [ClaseDeclaracion] [varchar](2) NOT NULL DEFAULT (''),
  [ClaseInstrumento] [smallint] NOT NULL DEFAULT (0),
  [NIFEmisor] [varchar](9) NOT NULL DEFAULT (''),
  [CodigoISIN] [varchar](12) NOT NULL DEFAULT (''),
  [Epigrafe] [smallint] NOT NULL DEFAULT (0),
  [TitularMaster] [varchar](9) NOT NULL DEFAULT (''),
  [PaisContrapartida] [varchar](2) NOT NULL DEFAULT (''),
  [Titular] [varchar](9) NOT NULL DEFAULT (''),
  [NombreISIN] [varchar](50) NOT NULL DEFAULT (''),
  [NumeroSaldoInicial] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroEntradas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroEntradasSinPrecio] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSalidas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSalidasSinPrecio] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroSaldoFinal] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSaldoInicial] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteEntradas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSalidas] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteSaldoFinal] [numeric](18, 2) NOT NULL DEFAULT (0),
  [NumeroCupon] [numeric](18, 2) NOT NULL DEFAULT (0),
  [ImporteCupon] [numeric](18, 2) NOT NULL DEFAULT (0),
  [CuentaValores] [varchar](20) NOT NULL DEFAULT (''),
  CONSTRAINT [PK_VAN_AGREGADOS] PRIMARY KEY NONCLUSTERED ([SkValor], [IDEntrada], [SECEntrada])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [ID_ENTRADA]
  ON [fvn].[VAN_AGREGADOS] ([IDEntrada], [SECEntrada])
  ON [PRIMARY]
GO