﻿CREATE TABLE [dbo].[del_table_check] (
  [id] [int] IDENTITY,
  [column2] [varchar](50) NULL,
  [datetime] [datetime] NULL,
  CONSTRAINT [PK_del_table_check_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

CREATE UNIQUE INDEX [UK_del_table_check_datetime]
  ON [dbo].[del_table_check] ([datetime])
  ON [PRIMARY]
GO