﻿CREATE TABLE [dbo].[MSSQL_TemporalHistoryFor_110623437] (
  [DeptID] [int] NOT NULL,
  [DeptName] [varchar](5) NULL,
  [ManagerID] [int] NULL,
  [ParentDeptID] [int] NULL,
  [SysStartTime] [datetime2] NOT NULL,
  [SysEndTime] [datetime2] NOT NULL
)
ON [PRIMARY]
GO

CREATE CLUSTERED INDEX [ix_MSSQL_TemporalHistoryFor_110623437]
  ON [dbo].[MSSQL_TemporalHistoryFor_110623437] ([SysEndTime], [SysStartTime])
  ON [PRIMARY]
GO