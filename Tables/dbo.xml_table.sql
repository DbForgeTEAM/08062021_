﻿CREATE TABLE [dbo].[xml_table] (
  [id] [int] IDENTITY,
  [column2] [xml] NULL,
  [column3] [xml] NULL,
  [column4] [xml] NULL,
  [column5] [xml] NULL,
  CONSTRAINT [PK_xml_table_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY]
GO