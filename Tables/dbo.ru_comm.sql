﻿CREATE TABLE [dbo].[ru_comm] (
  [id] [int] IDENTITY,
  [column2] [varchar](50) NULL,
  CONSTRAINT [PK_ru_comm_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

EXEC sys.sp_addextendedproperty N'MS_Description', 'Комментарий', 'SCHEMA', N'dbo', 'TABLE', N'ru_comm', 'COLUMN', N'id'
GO