﻿CREATE TABLE [dbo].[t_child1] (
  [id] [int] IDENTITY,
  [column2] [varchar](50) NULL,
  CONSTRAINT [PK_t_child_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO

ALTER TABLE [dbo].[t_child1] WITH NOCHECK
  ADD CONSTRAINT [FK_t_child_id] FOREIGN KEY ([id]) REFERENCES [dbo].[t_parent1] ([id])
GO