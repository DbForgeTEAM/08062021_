﻿CREATE TABLE [dbo].[all_date_types] (
  [id] [int] IDENTITY,
  [column1] [date] NULL,
  [column2] [datetime2] NULL,
  [column3] [datetime] NULL,
  [column4] [datetimeoffset] NULL,
  [column5] [time] NULL,
  [column6] [smalldatetime] NULL,
  CONSTRAINT [PK_all_date_types_id] PRIMARY KEY CLUSTERED ([id])
)
ON [PRIMARY]
GO