﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE VIEW [dbo].[view3] 
WITH SCHEMABINDING
AS SELECT
  sd.id, sd.column2, sd.datetime  
FROM dbo.del_table_check sd
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE UNIQUE CLUSTERED INDEX [UK_view3_id]
  ON [dbo].[view3] ([id])
  ON [PRIMARY]
GO

CREATE INDEX [IDX_view3_datetime]
  ON [dbo].[view3] ([datetime])
  INCLUDE ([column2])
  ON [PRIMARY]
GO