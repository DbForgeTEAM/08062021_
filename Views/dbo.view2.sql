﻿SET QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
CREATE VIEW [dbo].[view2] 
AS SELECT adc.pk_int
      ,adc.bigint
      ,adc.smallint
      ,adc.tinyint
      ,adc.bit
      ,adc.dec
      ,adc.[dec(25,9)]
      ,adc.[decimal(38,20)]
      ,adc.numeric
      ,adc.[numeric(38,3)]
      ,adc.money
      ,adc.smallmoney
      ,adc.float
      ,adc.real
      ,adc.datetime
      ,adc.smalldatetime
      ,adc.char
      ,adc.[char(5)]
      ,adc.varchar
      ,adc.[varchar(50)]
      ,adc.[varchar(max)]
      ,adc.text
      ,adc.nchar
      ,adc.[nchar(10)]
      ,adc.nvarchar
      ,adc.[nvarchar(1000)]
      ,adc.[nvarchar(max)]
      ,adc.ntext
      ,adc.binary
      ,adc.[binary(1234)]
      ,adc.varbinary
      ,adc.[varbinary(max)]
      ,adc.image
      ,adc.xml
      ,adc.sql_variant
      ,adc.sysname
      ,adc.uniqueidentifier
      ,adc.[binary varying]
      ,adc.[char varying]
      ,adc.hierachyid
      ,adc.geography
      ,adc.geometry
      ,adc.datetime2
      ,adc.datetimeoffset
      ,adc.date
      ,adc.time
      ,adc.rowversion FROM all_datatypes_copy adc
GO